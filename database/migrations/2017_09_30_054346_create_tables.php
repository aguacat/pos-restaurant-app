<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTables extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->decimal('capacity');
            $table->timestamps();
        });

        Schema::create('table_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('open')->default(true);

            $table->integer('id_table')->unsigned();

            $table->foreign('id_table')
                ->references('id')->on('tables')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('tables');
        Schema::dropIfExists('table_accounts');
        Schema::enableForeignKeyConstraints();
    }
}
