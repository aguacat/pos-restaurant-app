<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoices extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('invoice_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_type');
            $table->boolean('status')->default(true);
            $table->boolean('has_tax')->default(true);
            $table->boolean('rnc_required')->default(true);

            //Foreign Keys
            $table->integer('id_reference')->unsigned();

            //Foreign Keys Definition
            $table->foreign('id_reference')
                ->references('id')->on('references')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('NCF');
            $table->string('nombre');
            $table->string('RNC')->nullable();
            $table->boolean('takeout')->default(false);
            $table->boolean('final_consumer')->default(true);
            $table->decimal('sub_total')->default(0.00);
            $table->decimal('taxes')->default(0.00);
            $table->decimal('taxes_10')->default(0.00);
            $table->decimal('total')->default(0.00);
            $table->decimal('amount_paid')->default(0.00);
            $table->decimal('amount_to_return')->default(0.00);
            $table->string('status');

            //Foreign Keys
            $table->integer('id_invoice_type')->unsigned();

            //Foreign Keys Definition
            $table->foreign('id_invoice_type')
                ->references('id')->on('invoice_types')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('invoice_details', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('quantity');
            $table->decimal('price');
            $table->string('status');

            //Foreign Keys
            $table->integer('id_invoice')->unsigned();
            $table->integer('id_food')->unsigned();

            //Foreign Keys Definition
            $table->foreign('id_invoice')
                ->references('id')->on('invoices')
                ->onDelete('cascade');

            $table->foreign('id_food')
                ->references('id')->on('foods')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('invoice_details');
        Schema::dropIfExists('invoices');
        Schema::dropIfExists('invoice_types');
        Schema::enableForeignKeyConstraints();
    }
}
