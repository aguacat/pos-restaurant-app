<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrders extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('order_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_status');
            $table->timestamps();
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('date');
            $table->string('food_comment')->nullable();
            $table->boolean('is_closed')->default(false);

            //Foreign Keys
            $table->integer('id_food')->unsigned();
            $table->integer('id_order_status')->unsigned();
            $table->integer('id_table_account')->unsigned();

            //Foreign Keys Definition
            $table->foreign('id_food')
                ->references('id')->on('foods')
                ->onDelete('cascade');
            $table->foreign('id_order_status')
                ->references('id')->on('order_statuses')
                ->onDelete('cascade');
            $table->foreign('id_table_account')
                ->references('id')->on('table_accounts')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_statuses');
        Schema::enableForeignKeyConstraints();
    }
}
