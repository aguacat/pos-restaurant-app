<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFood extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('food_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('food_type');
            $table->string('cue_type');
            $table->string('active');
            $table->timestamps();
        });


        Schema::create('foods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('ingredients');
            $table->decimal('price');
            $table->decimal('cost');
            $table->integer('actual_quantity')->default(0);
            $table->integer('minimal_quantity')->default(1);
            $table->boolean('active')->default(1);

            //Foreign Keys
            $table->integer('id_food_type')->unsigned();

            //Foreign Keys Definition
            $table->foreign('id_food_type')
                ->references('id')->on('food_types')
                ->onDelete('cascade');

            $table->timestamps();
        });

        DB::statement("ALTER TABLE foods ADD image LONGBLOB");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('food_types');
        Schema::dropIfExists('foods');
        Schema::enableForeignKeyConstraints();
    }
}
