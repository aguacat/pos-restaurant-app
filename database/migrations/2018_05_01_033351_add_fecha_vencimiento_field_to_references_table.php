<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFechaVencimientoFieldToReferencesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('references', function (Blueprint $table) {
            $table->date('fecha_vencimiento')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('references', function (Blueprint $table) {
            $table->dropColumn('fecha_vencimiento');
        });
    }
}
