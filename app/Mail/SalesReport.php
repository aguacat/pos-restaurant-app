<?php

namespace App\Mail;

use App\Invoice;
use App\Reference;
use App\InvoiceType;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Carbon;
use Illuminate\Queue\SerializesModels;

class SalesReport extends Mailable
{
    use Queueable, SerializesModels;

    public $report;

    /**
     * Create a new message instance.
     */
    public function __construct()
    {
        $carbon = new Carbon('America/Santo_Domingo');
        $carbon->subMonth();

        $mesesDelAnio = [
            'January' => 'Enero',
            'February' => 'Febrero',
            'March' => 'Marzo',
            'April' => 'Abril',
            'May' => 'Mayo',
            'June' => 'Junio',
            'July' => 'Julio',
            'August' => 'Agosto',
            'September' => 'Septiembre',
            'October' => 'Octubre',
            'November' => 'Noviembre',
            'December' => 'Diciembre',
        ];

        $this->report['url'] = url('/').'/monthlysales/'.$carbon->year;
        $this->report['month'] = $mesesDelAnio[$carbon->format('F')];

        $startDate = new Carbon('first day of ' . $carbon->format('F'));
        $endDate = new Carbon('last day of ' . $carbon->format('F'));

        $totals = Invoice::SalesByType($startDate, $endDate);

        foreach($totals as $total) {
            $reference = Reference::where('header', $total->NCF)->first();
            $invoice_type = InvoiceType::where('id_reference', (string)$reference->id)->first();
            $total->NCF = $invoice_type->invoice_type . ' - ' . $total->NCF;
            $total->sub_total = 'RD$ '.number_format($total->sub_total, 2, '.', ',') ;
            $total->taxes = 'RD$ '.number_format($total->taxes, 2, '.', ',') ;
            $total->taxes_10 = 'RD$ '.number_format($total->taxes_10, 2, '.', ',') ;
            $total->total = 'RD$ '.number_format($total->total, 2, '.', ',') ;
        }

        $this->report['totals'] = $totals;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.sales_report')
            ->from('brunchcafebistro@gmail.com', 'SistemaBrunchCafe')
            ->subject('Reporte Ventas | Brunch Café')
            ->with('invoice', $this->report);
    }
}
