<?php

namespace App\Mail;

use App\InvoiceType;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Invoice extends Mailable {
    use Queueable, SerializesModels;

    public $invoice;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Invoice $invoice) {
        $invoice_detail = array();

        foreach ($invoice->invoice_detail() as $detail) {
            $detail['food'] = $detail->food()->first()->name;
            array_push($invoice_detail, $detail);
        }

        //Adding invoice Detail to Invoice.
        $invoice['invoice_detail'] = $invoice_detail;

        //Adding invoice Type Name to Invoice.
        $invoice['type'] = InvoiceType::find($invoice->id_invoice_type)->invoice_type;

        //Cleaning NCF
        $invoice->NCF = str_replace('[{"NCF":"', '', $invoice->NCF);
        $invoice->NCF = str_replace('"}]', '', $invoice->NCF);

        $this->invoice = $invoice;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->markdown('emails.invoice')
            ->from('brunchcafebistro@gmail.com', 'SistemaBrunchCafe')
            ->subject('Factura | Brunch Café')
            ->with('invoice', $this->invoice);
    }
}
