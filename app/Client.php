<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model {
    protected $fillable = ['email', 'first_name', 'last_name', 'birthdate', 'cell_number', 'phone', 'RNC', 'comercial_name', 'status'];
    protected $table = 'clients';
}