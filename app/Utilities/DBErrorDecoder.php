<?php

namespace App\Utilities;

class DBErrorDecoder {
    public $error_number;

    public function __construct($error_number) {
        $this->error_number = $error_number;
    }

    public function getMessage() {
        switch ($this->error_number) {
            case '23502':
                return "An not_null_violation occurred, check data";
                break;
            case '23503':
                return "An foreign_key_violation occurred, check data";
                break;
            case '23505':
                return "Información existente en la base de datos"; //"An unique_violation occurred, check data";
                break;
            case '42703':
                return "An undefined_column occurred, check data";
                break;
            default:
                return $this->error_number;
                break;
        }
    }
}