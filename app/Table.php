<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Table extends Model {
    protected $fillable = ['name', 'capacity'];
    protected $table = 'tables';

    public function scopeNumberOfAccountsPerTable($query) {
        return $query->select('tables.id', 'tables.name', DB::raw('COUNT(table_accounts.id) as Cuentas'))
            ->leftJoin('table_accounts', function ($join) {
                $join->on('table_accounts.id_table', 'tables.id')
                    ->on('table_accounts.open', DB::raw(true));
            })
            ->groupBy('tables.id', 'tables.name')
            ->orderBy('tables.id')
            ->get();
    }
}
