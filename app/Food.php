<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model {
    protected $fillable = ['name', 'ingredients', 'price', 'cost', 'actual_quantity', 'minimal_quantity', 'estado', 'id_food_type', 'created_at', 'updated_at', 'imagen'];
    protected $table = 'foods';

    public function food_type() {
        return $this->belongsTo(FoodType::class, 'id_food_type');
    }
}
