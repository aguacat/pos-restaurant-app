<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceType extends Model {
    protected $fillable = ['invoice_type', 'id_reference', 'status', 'has_tax', 'rnc_required'];
    protected $table = 'invoice_types';

    public function reference() {
        return $this->belongsTo(Reference::class, 'id_reference');
    }
}
