<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model {
    protected $fillable = ['quantity', 'price', 'status', 'id_invoice', 'id_food'];
    protected $table = 'invoice_details';

    public function food() {
        return $this->belongsTo(Food::class, 'id_food', 'id')->get();
    }
}
