<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodType extends Model {
    protected $fillable = ['food_type', 'status'];
    protected $table = 'food_types';
}
