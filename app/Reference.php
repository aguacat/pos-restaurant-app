<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Reference extends Model
{
    protected $fillable = ['header', 'description', 'sequence'];
    protected $table = 'references';

    public function scopeGetNextNCFNumber($query, $type)
    {
        $referenceType = InvoiceType::find($type);

        $query->where('references.id', $referenceType->id_reference)
            ->update(['references.sequence' => DB::raw('references.sequence + 1')]);

        return $query->select(DB::raw('CONCAT(header, LPAD(sequence, 8, \'0\')) as NCF'))
            ->where('invoice_types.id', $type)
            ->join('invoice_types', 'invoice_types.id_reference', 'references.id')
            ->first();
    }

    public function scopeSetPreviousNFCNumber($query, $type)
    {
        $referenceType = InvoiceType::find($type);

        $query->where('references.id', $referenceType->id_reference)
            ->update(['references.sequence' => DB::raw('references.sequence - 1')]);
    }
}
