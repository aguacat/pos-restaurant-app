<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'date',
        'NCF',
        'nombre',
        'RNC',
        'takeout',
        'final_consumer',
        'sub_total',
        'taxes',
        'taxes_10',
        'total',
        'amount_paid',
        'amount_to_return',
        'id_invoice_type',
    ];

    protected $table = 'invoices';

    public function invoice_detail()
    {
        return $this->hasMany(InvoiceDetail::class, 'id_invoice')->get();
    }

    public function type()
    {
        return $this->belongsTo(InvoiceType::class, 'id_invoice_type', 'id')->get();
    }

    public function scopeMonthlySales($query, $dateFrom, $dateTo)
    {
        // SELECT date as Fecha,
        //         CASE WHEN LENGTH(NCF) > 19 THEN SUBSTRING(SUBSTRING(NCF, -22, 22), 1, 19) ELSE  NCF END as NCF,
        //         nombre,
        //         RNC,
        //         'Venta' as 'TipoRecibo',
        //         total as 'Venta Bruta',
        //         sub_total as 'Sub-Total',
        //         taxes as 'ITIBIS',
        //         taxes_10 as '10% Ley',
        //         amount_paid as 'Total Pagado',
        //         amount_to_return as 'Devuelto',
        //         'Efectivo' as 'Tipo de Pago'
        // FROM invoices
        // WHERE (date BETWEEN '2018-01-01 0:00:00' AND '2018-01-30 00:00:00');

        return $query->select(
            'date',
            DB::raw('CASE WHEN LENGTH(NCF) > 19 THEN SUBSTRING(SUBSTRING(NCF, -22, 22), 1, 19) ELSE NCF END as NCF'),
            'nombre',
            'RNC',
            DB::raw('"Venta" as "TipoRecibo"'),
            'total',
            'sub_total',
            'taxes',
            'taxes_10',
            'amount_paid',
            'amount_to_return',
            DB::raw('"Efectivo" as "Tipo de Pago"')
        )
        ->whereBetween('date', [$dateFrom, $dateTo])
        ->get();
    }

    public function scopeSalesByType($query, $dateFrom, $dateTo)
    {
        // SELECT  LEFT(invoices.NCF, 3) as NCF, 
        //         count(invoices.`id`),
        //         sum(invoices.sub_total),
        //         sum(invoices.taxes), 
        //         sum(invoices.taxes_10),
        //         sum(invoices.total) 
        // FROM invoices, 
        //     (SELECT header FROM `references` WHERE header LIKE 'B%') AS types
        // WHERE (date BETWEEN '2018-07-01 0:00:00' AND '2018-07-31 00:00:00')
        // AND LEFT(invoices.NCF, 3) = types.header
        // GROUP BY LEFT(invoices.NCF, 3);  
        return DB::select(DB::raw("SELECT LEFT(invoices.NCF, 3) as NCF, count(invoices.`id`) as conteo, sum(invoices.sub_total) as sub_total, sum(invoices.taxes) as taxes, sum(invoices.taxes_10) as taxes_10, sum(invoices.total) as total FROM invoices, (SELECT header FROM `references` WHERE header LIKE 'B%') AS types WHERE (date BETWEEN '$dateFrom' AND '$dateTo') AND LEFT(invoices.NCF, 3) = types.header GROUP BY LEFT(invoices.NCF, 3)"));
    }
}