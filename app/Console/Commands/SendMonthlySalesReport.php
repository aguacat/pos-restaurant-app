<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendMonthlySalesReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:monthlysalesreport';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Monthly Sales Report';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Mail::to('luisa.gbay95@gmail.com')
            ->cc(['mulloa1101@gmail.com', 'yanilkabaez@gmail.com'])
            ->send(new \App\Mail\SalesReport());

        Mail::to('lukasgomez@me.com')->send(new \App\Mail\SalesReport());
    }
}
