<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model {
    protected $fillable = ['order_status', 'status'];
    protected $table = 'order_statuses';
}
