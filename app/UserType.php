<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model {
    protected $fillable = ['user_type', 'active'];
    protected $table = 'user_types';
}
