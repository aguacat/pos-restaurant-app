<?php

namespace App\Brunch\Transformers;

class InvoiceTransformer extends Transformer
{
    /**
     * @param $item
     * 
     * @return array
     */
    public function transform($invoice) {
        return [
            'fecha' => (string)$invoice->date,
            'ncf' => $invoice->NCF,
            'nombre' => $invoice->nombre,
            'rnc' => $invoice->RNC ?: 'N/A',
            'Tipo Recibo' => 'Venta',
            'Venta Bruta' => (float)$invoice->total,
            'Sub-Total' => (float)$invoice->sub_total,
            '10% Ley' => (float)$invoice->taxes_10,
            'Total Pagado' => (float)$invoice->amount_paid,
            'Devuelto' => (float)$invoice->amount_to_return,
            'Tipo de Pago' => 'Efectivo'
        ];
    }
}