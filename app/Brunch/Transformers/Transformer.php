<?php

namespace App\Brunch\Transformers;

abstract class Transformer
{
    /**
     * Transforma una colección de items.
     * 
     * @param $items
     * 
     * @return array
     */
    public function transformCollection(array $items)
    {
        return array_map([$this, 'transform'], $items);
    }

    abstract public function transform($item);
}