<?php

namespace App\Http\Controllers;

use App\OrderStatus;

class OrderStatusController extends Controller {
    public $data;

    public function __construct() {
        $this->data['order_status_list'] = OrderStatus::orderBy('id')->get();
    }

    public function index() {
        return view('order_status.show', $this->data);
    }

    public function store() {
        $this->validate(request(), [
            'order_status' => 'required'
        ]);

        $order_status = new OrderStatus();
        $order_status->order_status = request('order_status');
        $order_status->save();

        return redirect('/order_status');
    }

    public function edit(OrderStatus $order_status) {
        $this->data['current_item'] = $order_status;

        return view('order_status.edit', $this->data);
    }

    public function update(OrderStatus $order_status) {
        $this->validate(request(), [
            'order_status' => 'required'
        ]);

        $order_status->order_status = request('order_status');
        $order_status->save();

        return redirect('/order_status');
    }

    public function destroy(OrderStatus $order_status) {
        return redirect('/order_status');
    }
}
