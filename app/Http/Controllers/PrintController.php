<?php

namespace App\Http\Controllers;

use App\Invoice;
use Illuminate\Support\Facades\Mail;

class PrintController extends Controller
{
    public $data;

    public function __construct()
    {
        $this->data['invoice_list'] = Invoice::orderBy('created_at', 'desc')->get();
    }

    public function index()
    {
        return view('print.show', $this->data);
    }

    public function details($id)
    {
        $invoice = Invoice::find($id);
        $invoice_detail = [];

        foreach ($invoice->invoice_detail() as $detail) {
            $detail['food'] = $detail->food()->first()->name;
            array_push($invoice_detail, $detail);
        }

        return response()->json([
            'invoice' => $invoice,
            'invoice_type' => $invoice->type()->first()->invoice_type,
            'details' => $invoice_detail,
        ]);
    }

    public function mail($id)
    {
        $this->validate(request(), [
            'invoice_mail' => 'required',
        ]);

        $invoice = Invoice::find($id);

        Mail::to(request()->invoice_mail)->send(new \App\Mail\Invoice($invoice));

        return response()->json(true);
    }

    public function print($id)
    {
        $invoice = Invoice::find($id);

        return response()->json([
            'invoice' => $invoice,
            'items' => $invoice->invoice_detail(),
            'invoice_type' => $invoice->type()->first()->invoice_type,
        ], 200);
    }
}
