<?php

namespace App\Http\Controllers;

use App\User;
use App\UserType;

class UserController extends Controller {
    public $data;

    public function __construct() {
        $this->data['user_list'] = User::orderBy('id')->get();
        $this->data['user_type_list'] = UserType::where('active', true)->orderBy('id')->get();
    }

    public function index() {
        return view('user.show', $this->data);
    }

    public function store() {
        $this->validate(request(), [
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'      => 'required',
            'password'   => 'required|min:6|confirmed',
            'user_type'  => 'required',
        ]);

        $user = new User();
        $user->first_name = request('first_name');
        $user->last_name = request('last_name');
        $user->email = request('email');
        $user->password = bcrypt(request('password'));
        $user->id_user_type = request('user_type');
        $user->save();

        return redirect('/user');
    }

    public function edit(User $user) {
        $this->data['current_item'] = $user;

        return view('user.edit', $this->data);
    }

    public function update(User $user) {
        $this->validate(request(), [
            'first_name' => 'required',
            'last_name'  => 'required',
            'email'      => 'required',
            'password'   => 'nullable|min:6|confirmed',
            'user_type'  => 'required',
        ]);

        $user->first_name = request('first_name');
        $user->last_name = request('last_name');
        $user->email = request('email');

        if (request()->has('password')) {
            $user->password = bcrypt(request('password'));
        }

        $user->id_user_type = request('user_type');
        $user->save();

        return redirect('/user');
    }

    public function destroy(User $user) {
        //
    }
}
