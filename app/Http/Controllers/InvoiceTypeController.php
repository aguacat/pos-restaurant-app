<?php

namespace App\Http\Controllers;

use App\InvoiceType;
use App\Reference;
use Illuminate\Http\Request;

class InvoiceTypeController extends Controller {
    public $data;

    public function __construct() {
        $this->data['invoice_types_list'] = InvoiceType::orderBy('id')->get();
        $this->data['references_list'] = Reference::orderBy('id')->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('invoice_type.show', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate(request(), [
            'invoice_type' => 'required',
            'reference'    => 'required'
        ]);

        $invoice_type = new InvoiceType();
        $invoice_type->invoice_type = request('invoice_type');
        $invoice_type->id_reference = request('reference');
        $invoice_type->has_tax = request('has_tax') ? true : false;
        $invoice_type->rnc_required = request('rnc_required') ? true : false;
        $invoice_type->save();

        return redirect('/invoice_type');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvoiceType $invoiceType
     * @return \Illuminate\Http\Response
     */
    public function show(InvoiceType $invoiceType) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InvoiceType $invoiceType
     * @return \Illuminate\Http\Response
     */
    public function edit(InvoiceType $invoiceType) {
        $this->data['current_item'] = $invoiceType;

        return view('invoice_type.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\InvoiceType $invoiceType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvoiceType $invoiceType) {
        $this->validate(request(), [
            'invoice_type' => 'required',
            'reference'    => 'required'
        ]);

        $invoiceType->invoice_type = request('invoice_type');
        $invoiceType->id_reference = request('reference');
        $invoiceType->has_tax = request('has_tax') ? true : false;
        $invoiceType->rnc_required = request('rnc_required') ? true : false;
        $invoiceType->save();

        return redirect('/invoice_type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvoiceType $invoiceType
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvoiceType $invoiceType) {
        //
    }
}
