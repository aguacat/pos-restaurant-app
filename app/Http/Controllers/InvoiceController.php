<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\InvoiceDetail;
use App\InvoiceType;
use App\Order;
use App\OrderStatus;
use App\Reference;
use App\TableAccount;
use App\Utilities\DBErrorDecoder;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    public $data;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['open_accounts_list'] = TableAccount::OpenOrdersWithTableNotDeliveredNorInvoiced();
        $this->data['invoice_type_list'] = InvoiceType::where('status', true)->orderBy('id')->get();

        return view('invoice.show', $this->data);
    }

    public function getAccountOrderDetail(TableAccount $account)
    {
        $account_order_detail_list = Order::ActiveOrdersFromAccount($account->id);
        $account_order_totals = Order::ActiveOrdersFromAccountTotal($account->id);

        return response()->json(['orderDetails' => $account_order_detail_list, 'totals' => $account_order_totals]);
    }

    public function checkForTax(InvoiceType $invoice_type)
    {
        return response()->json(['has_tax' => $invoice_type->has_tax, 'rnc_required' => $invoice_type->rnc_required]);
    }

    public function performProcessInvoice(TableAccount $account)
    {
        DB::beginTransaction();

        try {
            $invoice = new Invoice();
            $invoice->date = now();
            $invoice->nombre = request('invoice_name');
            $invoice->RNC = request('rnc') ? request('rnc') : null;
            $invoice->id_invoice_type = request('invoice_type');

            $invoice->sub_total = request('sub_total');
            $invoice->taxes = request('taxes');
            $invoice->taxes_10 = request('taxes_10');
            $invoice->total = request('total');
            $invoice->amount_paid = request('amount_paid');
            $invoice->amount_to_return = str_replace(',', '', request('amount_to_return'));
            $invoice->status = 'PAGADO';

            //Calcular NCF
            $numero_comprobante = Reference::GetNextNCFNumber(request('invoice_type'));
            $invoice->NCF = $numero_comprobante->NCF;

            $invoice->save();

            //Llenar los Items
            foreach (Order::ActiveOrdersFromAccount($account->id) as $currentItem) {
                $item = new InvoiceDetail();
                $item->id_invoice = $invoice->id;
                $item->id_food = $currentItem->id;
                $item->price = $currentItem->price;

                //Adding Facturado to whatever actual Order Status:
                $newOrderStatus = OrderStatus::where('order_status', $currentItem->order_status.' - Facturado')->first();
                $item->status = $newOrderStatus->order_status;
                $item->quantity = 1;

                $item->save();

                $order = Order::find($currentItem->order_id);
                $order->id_order_status = $newOrderStatus->id;
                $order->save();
            }

            DB::commit();

            return response()->json(['invoice' => $invoice, 'items' => $invoice->invoice_detail()], 200);
        } catch (QueryException $ex) {
            $error = new DBErrorDecoder($ex->getCode());

            DB::rollback();

            return response()->json('Notificación del Sistema: '.$error->getMessage(), 409);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Order $order
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Order $order
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Order               $order
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Order $order
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
    }
}
