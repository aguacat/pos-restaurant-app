<?php

namespace App\Http\Controllers;

use App\UserType;

class UserTypeController extends Controller {
    public $data;

    public function __construct() {
        $this->data['user_type_list'] = UserType::where('active', true)->orderBy('id')->get();
    }

    public function index() {
        return view('user_type.show', $this->data);
    }

    public function store() {
        $this->validate(request(), [
            'user_type' => 'required'
        ]);

        $user_type = new UserType();
        $user_type->user_type = request('user_type');
        $user_type->active = true;
        $user_type->save();

        return redirect('/user_type');
    }

    public function edit(UserType $user_type) {
        $this->data['current_item'] = $user_type;

        return view('user_type.edit', $this->data);
    }

    public function update(UserType $user_type) {
        $this->validate(request(), [
            'user_type' => 'required'
        ]);

        $user_type->user_type = request('user_type');
        $user_type->save();

        return redirect('/user_type');
    }

    public function destroy(UserType $user_type) {
        $user_type->active = false;
        $user_type->save();

        return redirect('/user_type');
    }
}
