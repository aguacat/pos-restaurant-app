<?php

namespace App\Http\Controllers;

use App\Food;
use App\FoodType;
use Intervention\Image\Facades\Image;

class FoodController extends Controller {
    public $data;

    public function __construct() {
        $this->data['food_list'] = Food::where('active', true)->orderBy('id')->get();
        $this->data['food_type_list'] = FoodType::where('active', true)->orderBy('id')->get();
    }

    public function index() {
        return view('food.show', $this->data);
    }

    public function store() {
        $this->validate(request(), [
            'name'        => 'required',
            'ingredients' => 'required',
            'price'       => 'required|numeric',
            'cost'        => 'required|numeric',
            'food_type'   => 'required',
            'image '      => 'nullable|file|max:1024',
        ]);

        $food = new Food();
        $food->name = request('name');
        $food->ingredients = request('ingredients');
        $food->price = request('price');
        $food->cost = request('cost');
        $food->id_food_type = request('food_type');
        $food->active = true;

        if (request()->has('image')) {
            //Convertir Imagen a Base64:
            $convertedImage = (string) Image::make(request()->file('image'))->rotate(90);
            encode('data-url');
            $food->image = $convertedImage;;
        }

        $food->active = true;
        
        try {
            $food->save();
        } catch (\Illuminate\Database\QueryException $ex) {
            $error = new DBErrorDecoder($ex->getCode());

            return back()->withErrors(["Error: " . $error->getMessage()]);
        }

        return redirect('/food');
    }

    public function retrievePicture(Food $food) {
        return $food->image;
    }

    public function edit(Food $food) {
        $this->data['current_item'] = $food;

        return view('food.edit', $this->data);
    }

    public function update(Food $food) {
        $this->validate(request(), [
            'name'        => 'required',
            'ingredients' => 'required',
            'price'       => 'required|numeric',
            'cost'        => 'required|numeric',
            'food_type'   => 'required',
            'image '      => 'nullable|file|max:1024',
        ]);

        $food->name = request('name');
        $food->ingredients = request('ingredients');
        $food->price = request('price');
        $food->cost = request('cost');
        $food->id_food_type = request('food_type');

        if (request()->has('image')) {
            //Convertir Imagen a Base64:
            $convertedImage = (string) Image::make(request()->file('image'))->resize(640, 640)->encode('data-url');
            $food->image = $convertedImage;;
        }

        $food->active = request('active') ? true : false;
        
        try {
            $food->save();
        } catch (\Illuminate\Database\QueryException $ex) {
            $error = new DBErrorDecoder($ex->getCode());

            return back()->withErrors(["Error: " . $error->getMessage()]);
        }

        return redirect('/food');
    }

    public function destroy(Food $food) {
        if(is_null($food))
            return redirect('/food');

        $food->active = false;
        $food->save();

        return redirect('/food');
    }
}
