<?php

namespace App\Http\Controllers;

class DashboardController extends Controller {
    public $data;

    public function __construct() {
        $this->data = array();
    }

    public function index() {
        return view('dashboard.show', $this->data);
    }
}
