<?php

namespace App\Http\Controllers;

use App\Table;
use App\Utilities\DBErrorDecoder;

class TableController extends Controller {
    public $data;

    public function __construct() {
        $this->data['table_list'] = Table::orderBy('id')->get();
    }

    public function index() {
        return view('table.show', $this->data);
    }

    public function store() {
        $this->validate(request(), [
            'name'     => 'required',
            'capacity' => 'required|numeric'
        ]);

        $table = new Table();
        $table->name = request('name');
        $table->capacity = request('capacity');
        $table->save();

        return redirect('/table');
    }

    public function edit(Table $table) {
        $this->data['current_item'] = $table;

        return view('table.edit', $this->data);
    }

    public function update(Table $table) {
        $this->validate(request(), [
            'name'     => 'required',
            'capacity' => 'required|numeric'
        ]);

        $table->name = request('name');
        $table->capacity = request('capacity');
        $table->save();

        return redirect('/table');
    }

    public function destroy(Table $table) {
        try {
            $table->delete();
        } catch (\Illuminate\Database\QueryException $ex) {
            $error = new DBErrorDecoder($ex->getCode());

            return back()->withErrors(["Error: " . $error->getMessage()]);
        }

        return redirect('/table');
    }
}
