<?php

namespace App\Http\Controllers;

use App\Food;
use App\Order;
use App\Table;
use App\TableAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller {
    public $data;

    public function __construct() {
        $this->data['tables'] = Table::orderBy('id')->get();
        $this->data['accounts_number'] = Table::NumberOfAccountsPerTable();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('order.show', $this->data);
    }

    public function ordersByTable(Table $table) {
        $this->data['current_item'] = $table;
        $this->data['food_list'] = Food::where('active', true)->orderBy('id_food_type')->orderBy('name')->get();
        $this->data['open_accounts'] = TableAccount::where([['open', true], ['id_table', $table->id]])->orderBy('id')->get();
        $this->data['table_orders'] = Order::TableOrders($table->id);

        return view('order.show', $this->data);
    }

    public function addAccountToTable(Table $table) {
        $this->validate(request(), [
            'account_name' => 'required'
        ]);

        $table_account = new TableAccount();
        $table_account->id_table = $table->id;
        $table_account->name = request('account_name');
        $table_account->save();

        return redirect('/order/' . $table->id . '/orderbytable');
    }

    public function addOrderToAccount() {
        // $this->validate(request(), [
        //     'food'       => 'required',
        //     'id_account' => 'required',
        //     'quantity'   => 'required|numeric|max:3'
        // ]);

        $validator = $this->add_order_to_account_rules(request()->all());   
        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        for($i=1; $i <= request('quantity'); $i++) {
            $order = new Order();
            $order->id_order_status = 1;
            $order->id_table_account = request('id_account');
            $order->id_food = request('food');
            $order->food_comment = request('food_comment') ? request('food_comment') : '';
            $order->save();
        }

        $account = TableAccount::find(request('id_account'));

        return redirect('/order/' . $account->id_table . '/accountDetail/' . $account->id);
    }

    public function accountDetail(Table $table, TableAccount $account) {
        $this->data['current_item'] = $table;
        $this->data['food_list'] = Food::where('active', true)->orderBy('id_food_type')->orderBy('name')->get();
        $this->data['open_accounts'] = TableAccount::where([['open', true], ['id_table', $table->id]])->orderBy('id')->get();
        $this->data['table_orders'] = Order::TableOrders($table->id);

        $this->data['showAccount'] = $account->id;

        return view('order.show', $this->data);
    }

    public function cancelOrderOfAccount(Order $order) {
        $order->id_order_status = 13;
        $order->is_closed = true;
        $order->save();

        $account = TableAccount::find($order->id_table_account);

        return response()->json(['id_table' => $account->id_table, 'id_account' => $account->id]);
    }

    public function closeAccount(TableAccount $account) {
        $account->open = false;
        $account->save();

        return response()->json(['id_table' => $account->id_table]);
    }

    public function closeAccountRedirect(TableAccount $account) {
        $account->open = false;
        $account->save();

        return redirect('/order/' . $account->id_table . '/orderbytable');
    }

    public function foodCue() {
        $this->data['pending_food_list'] = Order::Pending([1, 2, 3, 4]);
        $this->data['preparing_food_list'] = Order::Preparing([1, 2, 3, 4]);
        $this->data['ready_food_list'] = Order::Ready([1, 2, 3, 4]);

        return view('order.solid_cue', $this->data);
    }

    public function drinkCue() {
        $this->data['pending_food_list'] = Order::Pending([1, 2, 3, 4]);
        $this->data['preparing_food_list'] = Order::Preparing([1, 2, 3, 4]);
        $this->data['ready_food_list'] = Order::Ready([1, 2, 3, 4]);

        return view('order.liquid_cue', $this->data);
    }

    public function updateCueStatus($order, $status) {
        $currentOrder = Order::find($order);

        switch ($status) {
            case 'en_proceso':
                if ($currentOrder->id_order_status == 1) {
                    $currentOrder->id_order_status = 4;
                }

                if ($currentOrder->id_order_status == 2) {
                    $currentOrder->id_order_status = 4;
                }

                if ($currentOrder->id_order_status == 7) {
                    $currentOrder->id_order_status = 10;
                }

                if ($currentOrder->id_order_status == 8) {
                    $currentOrder->id_order_status = 10;
                }
                break;
            case 'lista':
                if ($currentOrder->id_order_status == 4) {
                    $currentOrder->id_order_status = 14;
                }

                if ($currentOrder->id_order_status == 10) {
                    $currentOrder->id_order_status = 15;
                }
                break;
            case 'entregada':
                if ($currentOrder->id_order_status == 14) {
                    $currentOrder->id_order_status = 5;
                }

                if ($currentOrder->id_order_status == 15) {
                    $currentOrder->id_order_status = 11;
                }
                break;
        }

        $currentOrder->save();

        return $currentOrder;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Order $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order) {
        //
    }

    /**
     * Custom Validation for this Controller
     */
    public function add_order_to_account_rules(array $data)
    {
        $messages = [
            'food.required'       => 'Debes elegir un item del menú.',
            'quantity.max'   => 'El máximo de elementos posible es 20.'
        ];
            
        $validator = Validator::make($data, [
            'food'       => 'required',
            'id_account' => 'required',
            'quantity'   => 'required|numeric|max:20'  
        ], $messages);
        
        return $validator;
    }
}
