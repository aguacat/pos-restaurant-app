<?php

namespace App\Http\Controllers;

use App\Reference;
use Carbon\Carbon;

class ReferenceController extends Controller
{
    public $data;

    public function __construct()
    {
        $this->data['reference_list'] = Reference::orderBy('id')->get();
    }

    public function index()
    {
        return view('reference.show', $this->data);
    }

    public function store()
    {
        $this->validate(request(), [
            'header' => 'required|unique:references',
            'description' => 'required',
            'start' => 'required|numeric',
            'end' => 'required|numeric',
        ]);

        $reference = new Reference();
        $reference->header = request('header');
        $reference->description = request('description');

        if (request()->filled('fecha_vencimiento')) {
            $reference->fecha_vencimiento = Carbon::createFromFormat('d/m/Y', request('fecha_vencimiento'))
                                                ->format('Y-m-d');
        }

        $reference->sequence = request('start');
        $reference->start = request('start');
        $reference->end = request('end');
        $reference->save();

        return redirect('/reference');
    }

    public function edit(Reference $reference)
    {
        $this->data['current_item'] = $reference;

        return view('reference.edit', $this->data);
    }

    public function update(Reference $reference)
    {
        $this->validate(request(), [
            'header' => 'required',
            'description' => 'required',
            'start' => 'required|numeric',
            'end' => 'required|numeric',
        ]);

        $reference->header = request('header');
        $reference->description = request('description');

        if (request()->filled('fecha_vencimiento')) {
            $reference->fecha_vencimiento = Carbon::createFromFormat('d/m/Y', request('fecha_vencimiento'))
                                            ->format('Y-m-d');
        }

        $reference->sequence = request('start');
        $reference->start = request('start');
        $reference->end = request('end');
        $reference->save();

        return redirect('/reference');
    }

    public function destroy(Reference $reference)
    {
        return redirect('reference');
    }
}
