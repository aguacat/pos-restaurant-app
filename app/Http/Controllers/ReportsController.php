<?php

namespace App\Http\Controllers;

use App\Invoice;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Brunch\Transformers\InvoiceTransformer;

class ReportsController extends Controller
{
    protected $invoiceTransformer;
    protected $titulos;

    public function __construct(InvoiceTransformer $invoiceTransformer)
    {
        $this->invoiceTransformer = $invoiceTransformer;

        $this->titulos = [
            'fecha' => 'Fecha',
            'NCF' => 'NCF',
            'nombre' => 'Nombre',
            'rnc' => 'RNC',
            'Tipo Recibo' => 'Tipo Recibo',
            'Venta Bruta' => 'Venta Bruta',
            'Sub-Total' => 'Sub-Total',
            '10% Ley' => '10% Ley',
            'Total Pagado' => 'Total Pagado',
            'Devuelto' => 'Devuelto',
            'Tipo de Pago' => 'Tipo de Pago'
        ];
    }

    public function mail()
    {
        Mail::to('lukasgomez@me.com')->send(new \App\Mail\Invoice($invoice));
        //Mail::to('lukasgomez@me.com')->send(new \App\Mail\Invoice($invoice));

        return response()->json(true);
    }

    function setDecimals($sheet) {
        // Sheet format
        $sheet->setColumnFormat([
            'F' => '#,##0.00',
            'G' => '#,##0.00',
            'H' => '#,##0.00',
            'I' => '#,##0.00',
            'J' => '#,##0.00',
        ]);

        return;
    }

    public function monthlySales($year)
    {
        Excel::create('Reporte de Recibos • Brunch Café Bistro • Año '.$year, function ($excel) use ($year) {
            $carbon = new Carbon('America/Santo_Domingo');
            $carbon->subMonth();

            // Set the title
            $excel->setTitle('Reporte de Ventas Mensuales '.date('Y'));

            // Chain the setters
            $excel->setCreator('Brunch Café System')
                    ->setCompany('Brunch Café Store');

            // Call them separately
            $excel->setDescription('A demonstration to change the file properties');

            if ($carbon->month >= 1) {
                //ENERO:
                $excel->sheet('Recibos Enero', function ($sheet) use ($year) {
                    $facturas = Invoice::MonthlySales($year.'-01-01', $year.'-01-31');
                    $facturas = $this->invoiceTransformer->transformCollection($facturas->all());

                    // Adding Titles
                    array_unshift($facturas, $this->titulos);

                    // Set Decimals;
                    $this->setDecimals($sheet);

                    // Sheet manipulation
                    $sheet->fromArray($facturas, null, 'A1', false, false);
                });
            }

            if ($carbon->month >= 2) {
                //FEBRERO:
                $excel->sheet('Recibos Febrero', function ($sheet) use ($year) {
                    $facturas = Invoice::MonthlySales($year.'-02-01', $year.'-02-28');
                    $facturas = $this->invoiceTransformer->transformCollection($facturas->all());

                    // Adding Titles
                    array_unshift($facturas, $this->titulos);

                    // Set Decimals;
                    $this->setDecimals($sheet);

                    // Sheet manipulation
                    $sheet->fromArray($facturas, null, 'A1', false, false);
                });
            }

            if ($carbon->month >= 3) {
                //MARZO:
                $excel->sheet('Recibos Marzo', function ($sheet) use ($year) {
                    $facturas = Invoice::MonthlySales($year.'-03-01', $year.'-03-31');
                    $facturas = $this->invoiceTransformer->transformCollection($facturas->all());

                    // Adding Titles
                    array_unshift($facturas, $this->titulos);

                    // Set Decimals;
                    $this->setDecimals($sheet);

                    // Sheet manipulation
                    $sheet->fromArray($facturas, null, 'A1', false, false);
                });
            }

            if ($carbon->month >= 4) {
                //ABRIL:
                $excel->sheet('Recibos Abril', function ($sheet) use ($year) {
                    $facturas = Invoice::MonthlySales($year.'-04-01', $year.'-04-31');
                    $facturas = $this->invoiceTransformer->transformCollection($facturas->all());

                    // Adding Titles
                    array_unshift($facturas, $this->titulos);

                    // Set Decimals;
                    $this->setDecimals($sheet);

                    // Sheet manipulation
                    $sheet->fromArray($facturas, null, 'A1', false, false);
                });
            }

            if ($carbon->month >= 5) {
                //MAYO:
                $excel->sheet('Recibos Mayo', function ($sheet) use ($year) {
                    $facturas = Invoice::MonthlySales($year.'-05-01', $year.'-05-31');
                    $facturas = $this->invoiceTransformer->transformCollection($facturas->all());

                    // Adding Titles
                    array_unshift($facturas, $this->titulos);

                    // Set Decimals;
                    $this->setDecimals($sheet);

                    // Sheet manipulation
                    $sheet->fromArray($facturas, null, 'A1', false, false);
                });
            }

            if ($carbon->month >= 6) {
                //JUNIO:
                $excel->sheet('Recibos Junio', function ($sheet) use ($year) {
                    $facturas = Invoice::MonthlySales($year.'-06-01', $year.'-06-30');
                    $facturas = $this->invoiceTransformer->transformCollection($facturas->all());

                    // Adding Titles
                    array_unshift($facturas, $this->titulos);

                    // Set Decimals;
                    $this->setDecimals($sheet);

                    // Sheet manipulation
                    $sheet->fromArray($facturas, null, 'A1', false, false);
                });
            }

            if ($carbon->month >= 7) {
                //JULIO:
                $excel->sheet('Recibos Julio', function ($sheet) use ($year) {
                    $facturas = Invoice::MonthlySales($year.'-07-01', $year.'-07-31');
                    $facturas = $this->invoiceTransformer->transformCollection($facturas->all());

                    // Adding Titles
                    array_unshift($facturas, $this->titulos);

                    // Set Decimals;
                    $this->setDecimals($sheet);

                    // Sheet manipulation
                    $sheet->fromArray($facturas, null, 'A1', false, false);
                });
            }

            if ($carbon->month >= 8) {
                //AGOSTO:
                $excel->sheet('Recibos Agosto', function ($sheet) use ($year) {
                    $facturas = Invoice::MonthlySales($year.'-08-01', $year.'-08-31');
                    $facturas = $this->invoiceTransformer->transformCollection($facturas->all());

                    // Adding Titles
                    array_unshift($facturas, $this->titulos);

                    // Set Decimals;
                    $this->setDecimals($sheet);

                    // Sheet manipulation
                    $sheet->fromArray($facturas, null, 'A1', false, false);
                });
            }

            if ($carbon->month >= 9) {
                //SEPTIEMBRE:
                $excel->sheet('Recibos Septiembre', function ($sheet) use ($year) {
                    $facturas = Invoice::MonthlySales($year.'-09-01', $year.'-09-30');
                    $facturas = $this->invoiceTransformer->transformCollection($facturas->all());

                    // Adding Titles
                    array_unshift($facturas, $this->titulos);

                    // Set Decimals;
                    $this->setDecimals($sheet);

                    // Sheet manipulation
                    $sheet->fromArray($facturas, null, 'A1', false, false);
                });
            }

            if ($carbon->month >= 10) {
                //OCTUBRE:
                $excel->sheet('Recibos Octubre', function ($sheet) use ($year) {
                    $facturas = Invoice::MonthlySales($year.'-10-01', $year.'-10-31');
                    $facturas = $this->invoiceTransformer->transformCollection($facturas->all());

                    // Adding Titles
                    array_unshift($facturas, $this->titulos);

                    // Set Decimals;
                    $this->setDecimals($sheet);

                    // Sheet manipulation
                    $sheet->fromArray($facturas, null, 'A1', false, false);
                });
            }

            if ($carbon->month >= 11) {
                //NOVIEMBRE:
                $excel->sheet('Recibos Noviembre', function ($sheet) use ($year) {
                    $facturas = Invoice::MonthlySales($year.'-11-01', $year.'-11-30');
                    $facturas = $this->invoiceTransformer->transformCollection($facturas->all());

                    // Adding Titles
                    array_unshift($facturas, $this->titulos);

                    // Set Decimals;
                    $this->setDecimals($sheet);

                    // Sheet manipulation
                    $sheet->fromArray($facturas, null, 'A1', false, false);
                });
            }

            if ($carbon->month >= 12) {
                //DICIEMBRE:
                $excel->sheet('Recibos Diciembre', function ($sheet) use ($year) {
                    $facturas = Invoice::MonthlySales($year.'-12-01', $year.'-12-31');
                    $facturas = $this->invoiceTransformer->transformCollection($facturas->all());

                    // Adding Titles
                    array_unshift($facturas, $this->titulos);

                    // Set Decimals;
                    $this->setDecimals($sheet);
                    
                    // Sheet manipulation
                    $sheet->fromArray($facturas, null, 'A1', false, false);
                });
            }
        })->download('xlsx');
    }
}
