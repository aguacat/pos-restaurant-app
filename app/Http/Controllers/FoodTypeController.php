<?php

namespace App\Http\Controllers;

use App\FoodType;

class FoodTypeController extends Controller {
    public $data;

    public function __construct() {
        $this->data['food_type_list'] = FoodType::where('active', true)->orderBy('id')->get();
    }

    public function index() {
        return view('food_type.show', $this->data);
    }

    public function store() {
        $this->validate(request(), [
            'food_type' => 'required',
            'cue_type' => 'required'
        ]);

        $food_type = new FoodType();
        $food_type->food_type = request('food_type');
        $food_type->cue_type = request('cue_type');
        $food_type->active = true;
        $food_type->save();

        return redirect('/food_type');
    }

    public function edit(FoodType $food_type) {
        $this->data['current_item'] = $food_type;

        return view('food_type.edit', $this->data);
    }

    public function update(FoodType $food_type) {
        $this->validate(request(), [
            'food_type' => 'required',
            'cue_type' => 'required'
        ]);

        $food_type->food_type = request('food_type');
        $food_type->cue_type = request('cue_type');
        $food_type->active = request('active') ? true : false;
        $food_type->save();

        return redirect('/food_type');
    }

    public function destroy(FoodType $food_type) {
        $food_type->active = false;
        $food_type->save();

        return redirect('/food_type');
    }
}
