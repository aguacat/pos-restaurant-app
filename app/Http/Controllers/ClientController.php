<?php

namespace App\Http\Controllers;

use App\Client;
use Carbon\Carbon;

class ClientController extends Controller {

    public $data;

    public function __construct() {
        $this->data['client_list'] = Client::where('status', 1)->orderBy('id')->get();
    }

    public function index() {
        return view('client.show', $this->data);
    }

    public function store() {
        $this->validate(request(), [
            'first_name'  => 'required_without:comercial_name',
            'last_name'   => 'required_without:comercial_name',
            'comercial_name' => 'required_without:first_name|required_without:last_name',
            'email'       => 'required|unique:clients',
            'RNC'         => 'required|unique:clients',
            'cell_number' => 'required',
        ]);

        $client = new Client();
        $client->email = request('email');
        $client->first_name = request('first_name');
        $client->last_name = request('last_name');
        $client->birthdate = request('birthdate') ? Carbon::createFromFormat('d/m/Y', request('birthdate'))->format('Y-m-d') : Carbon::now();
        $client->gender = request('gender');
        $client->cell_number = request('cell_number');
        $client->phone = request('phone');
        $client->RNC = request('RNC');
        $client->comercial_name = request('comercial_name');
        $client->save();

        return redirect('/client');
    }

    public function edit(Client $client) {
        $client->birthdate = Carbon::createFromFormat('Y-m-d', $client->birthdate)->format('d/m/Y');
        $this->data['current_item'] = $client;


        return view('client.edit', $this->data);
    }

    public function update(Client $client) {
        $this->validate(request(), [
            'first_name'  => 'required_without:comercial_name',
            'last_name'   => 'required_without:comercial_name',
            'comercial_name' => 'required_without:first_name|required_without:last_name',
            'email'       => 'required|unique:clients,email,' . $client->id,
            'RNC'         => 'required|unique:clients,RNC,' . $client->id,
            'cell_number' => 'required',
        ]);

        $client->email = request('email');
        $client->first_name = request('first_name');
        $client->last_name = request('last_name');
        $client->birthdate = request('birthdate') ? Carbon::createFromFormat('d/m/Y', request('birthdate'))->format('Y-m-d') : Carbon::now();
        $client->gender = request('gender');
        $client->cell_number = request('cell_number');
        $client->phone = request('phone');
        $client->RNC = request('RNC');
        $client->comercial_name = request('comercial_name');
        $client->save();

        return redirect('/client');
    }

    public function destroy(Client $client) {
        $client->status = false;
        $client->save();

        return redirect('/client');
    }
}
