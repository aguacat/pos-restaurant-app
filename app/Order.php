<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model {
    protected $fillable = ['date', 'name', 'is_closed', 'id_food', 'id_order_status', 'id_table'];
    protected $table = 'orders';

    public function scopeTableOrders($query, $table_id) {
        return $query->select('table_accounts.id', 'table_accounts.name', 'foods.name as food', 'order_statuses.order_status', 'order_statuses.id AS id_order_status', 'orders.id as id_order')
            ->join('foods', 'orders.id_food', 'foods.id')
            ->join('order_statuses', 'orders.id_order_status', 'order_statuses.id')
            ->join('table_accounts', 'orders.id_table_account', 'table_accounts.id')
            ->where([['table_accounts.id_table', $table_id], [DB::raw('DATE("orders.date") = CURDATE()')], ['is_closed', false]])
            ->get();
    }

    public function scopePending($query, $type) {
        return $query->select('orders.id', 'foods.name AS food', 'foods.image as image', 'food_types.food_type', 'food_types.cue_type', 'tables.name as table_name', 'table_accounts.name as account', 'orders.created_at', 'orders.food_comment', 'order_statuses.order_status')
            ->where('order_statuses.id', 1)
            ->orwhere('order_statuses.id', 2)
            ->orwhere('order_statuses.id', 7)
            ->orwhere('order_statuses.id', 8)
            ->join('foods', 'foods.id', 'orders.id_food')
            ->join('order_statuses', 'order_statuses.id', 'orders.id_order_status')
            ->join('table_accounts', 'table_accounts.id', 'orders.id_table_account')
            ->join('tables', 'tables.id', 'table_accounts.id_table')
            ->join('food_types', 'food_types.id', 'foods.id_food_type')
            ->orderBy('orders.id')
            ->get();
    }

    public function scopePreparing($query, $type) {
        return $query->select('orders.id', 'foods.name AS food', 'foods.image as image', 'food_types.food_type', 'food_types.cue_type', 'tables.name as table_name', 'table_accounts.name as account', 'orders.created_at', 'orders.food_comment', 'order_statuses.order_status')
            ->where('order_statuses.id', 4)
            ->orwhere('order_statuses.id', 10)
            ->join('foods', 'foods.id', 'orders.id_food')
            ->join('order_statuses', 'order_statuses.id', 'orders.id_order_status')
            ->join('table_accounts', 'table_accounts.id', 'orders.id_table_account')
            ->join('tables', 'tables.id', 'table_accounts.id_table')
            ->join('food_types', 'food_types.id', 'foods.id_food_type')
            ->orderBy('orders.id')
            ->get();
    }

    public function scopeReady($query, $type) {
        return $query->select('orders.id', 'foods.name AS food', 'foods.image as image', 'food_types.food_type', 'food_types.cue_type', 'tables.name as table_name', 'table_accounts.name as account', 'orders.created_at', 'orders.food_comment', 'order_statuses.order_status')
            ->where('order_statuses.id', 14)
            ->orwhere('order_statuses.id', 15)
            ->join('foods', 'foods.id', 'orders.id_food')
            ->join('order_statuses', 'order_statuses.id', 'orders.id_order_status')
            ->join('table_accounts', 'table_accounts.id', 'orders.id_table_account')
            ->join('tables', 'tables.id', 'table_accounts.id_table')
            ->join('food_types', 'food_types.id', 'foods.id_food_type')
            ->orderBy('orders.id')
            ->get();
    }

    public function scopeActiveOrdersFromAccount($query, $id_account) {
        return $query->select('orders.id AS order_id', 'foods.id', 'foods.name', 'foods.price', 'foods.cost', 'order_statuses.order_status')
            ->where([['orders.is_closed', false], ['id_table_account', $id_account], ['orders.id_order_status', '!=', 13]])
            ->join('foods', 'foods.id', 'orders.id_food')
            ->join('order_statuses', 'order_statuses.id', 'orders.id_order_status')
            ->join('table_accounts', 'orders.id_table_account', 'table_accounts.id')
            ->get();
    }

    public function scopeActiveOrdersFromAccountTotal($query, $id_account) {
        return $query->select(DB::raw('SUM(foods.cost) AS total_cost'), DB::raw('SUM(foods.price) AS total_price'))
            ->where([['orders.is_closed', false], ['id_table_account', $id_account], ['orders.id_order_status', '!=', 13]])
            ->join('foods', 'foods.id', 'orders.id_food')
            ->join('table_accounts', 'orders.id_table_account', 'table_accounts.id')
            ->first();
    }
}
