<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableAccount extends Model {
    protected $fillable = ['name', 'open', 'id_table'];
    protected $table = 'table_accounts';

    public function scopeOpenOrdersWithTable($query) {
        return $query->select('table_accounts.id', 'table_accounts.name as account', 'tables.name')
            ->join('tables', 'table_accounts.id_table', 'tables.id')
            ->where('table_accounts.open', true)
            ->get();
    }

    public function scopeOpenOrdersWithTableNotDeliveredNorInvoiced($query) {
//        SELECT DISTINCT table_accounts.id, table_accounts.name as account, tables.name
//        FROM table_accounts
//        JOIN tables ON table_accounts.id_table = tables.id
//        JOIN orders ON table_accounts.id = orders.id_table_account
//        WHERE table_accounts.open = TRUE
//        AND orders.id_order_status != 11;

        return $query->distinct()->select('table_accounts.id', 'table_accounts.name as account', 'tables.name')
            ->join('tables', 'table_accounts.id_table', 'tables.id')
            ->join('orders', 'table_accounts.id', 'orders.id_table_account')
            ->where('table_accounts.open', true)
            ->where('orders.id_order_status', '!=', 11)
            ->get();
    }
}
