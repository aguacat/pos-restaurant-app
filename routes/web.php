<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'DashboardController@index');

    /* Orders **/
    Route::resource('/order', 'OrderController');
    Route::get('/order/{table}/orderbytable', 'OrderController@ordersByTable');
    Route::get('/order/{table}/accountDetail/{account}', 'OrderController@accountDetail');
    Route::post('/order/{table}/accountDetail/{account}', 'OrderController@addOrderToAccount');
    Route::put('/order/{order}/cancelOrder', 'OrderController@cancelOrderOfAccount');
    Route::get('/order/{account}/closeAccount', 'OrderController@closeAccountRedirect');
    Route::put('/order/{account}/closeAccount', 'OrderController@closeAccount');
    Route::post('/order/{table}/addAccount', 'OrderController@addAccountToTable');
    Route::get('/order_cue/solidCue', 'OrderController@foodCue');
    Route::get('/order_cue/liquidCue', 'OrderController@drinkCue');
    Route::post('/order/{order}/cueStatus/{status}', 'OrderController@updateCueStatus');

    /* PrintController */
    Route::get('/print', 'PrintController@index');
    Route::get('/print/{id}', 'PrintController@details');
    Route::post('/print/{id}/mail', 'PrintController@mail');
    Route::post('/print/{id}/print', 'PrintController@print');

    /* InvoiceController **/
    Route::resource('/invoice', 'InvoiceController');
    Route::get('/invoice/{invoice}/print', 'InvoiceController@printInvoice');
    Route::post('/invoice/{account}/accountOrders', 'InvoiceController@getAccountOrderDetail');
    Route::post('/invoice/{invoice_type}/checkTax', 'InvoiceController@checkForTax');
    Route::post('/invoice/{account}/processInvoice', 'InvoiceController@performProcessInvoice');

    /* Invoice Types **/
    Route::resource('/invoice_type', 'InvoiceTypeController');

    /* Order Status **/
    Route::resource('/order_status', 'OrderStatusController');

    /* Clients **/
    Route::resource('/client', 'ClientController');

    /* References **/
    Route::resource('/reference', 'ReferenceController');

    /* User Type **/
    Route::resource('/user_type', 'UserTypeController');

    /* User**/
    Route::resource('/user', 'UserController');

    /* Food Types **/
    Route::get('/food_type', 'FoodTypeController@index');
    Route::post('/food_type', 'FoodTypeController@store');
    Route::get('/food_type/{food_type}/edit', 'FoodTypeController@edit');
    Route::put('/food_type/{food_type}', 'FoodTypeController@update');
    Route::delete('/food_type/{food_type}', 'FoodTypeController@destroy');

    /* Tables **/
    Route::get('/table', 'TableController@index');
    Route::post('/table', 'TableController@store');
    Route::get('/table/{table}/edit', 'TableController@edit');
    Route::put('/table/{table}', 'TableController@update');
    Route::delete('/table/{table}', 'TableController@destroy');

    /* Food **/
    Route::get('/food', 'FoodController@index');
    Route::post('/food', 'FoodController@store');
    Route::get('food/{food}', 'FoodController@retrievePicture');
    Route::get('/food/{food}/edit', 'FoodController@edit');
    Route::put('/food/{food}', 'FoodController@update');
    Route::delete('/food/{food}', 'FoodController@destroy');
});

/* Reports **/
Route::get('/monthlysales/{year}', 'ReportsController@monthlySales');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
