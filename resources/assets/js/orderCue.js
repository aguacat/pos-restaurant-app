
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Form from './core/Form.js';

const factura = new Vue({
    el: '#facturas',
    components: {

    },
    data: {
    },
    methods: {
        onSubmit() {
          this.cuenta_cliente.post('/generar_factura')
            .then(response => {
            	this.resetCuentaClienteForm();
                console.log(response);
            	this.factura.numero_factura = response.cuenta_cliente.numero_factura;
                this.factura.nombre = response.cliente.nombres + ' ' + response.cliente.apellidos;
                this.factura.hora_apertura = response.cuenta_cliente.created_at;
                this.factura.punto_servicio = response.barbero.nombres + ' ' + response.barbero.apellidos;
                this.factura.cajero = response.cajero.nombres + ' ' + response.cajero.apellidos;
                this.producto.cuenta_cliente = response.cuenta_cliente.id
                this.producto.cantidad = 1;
            });
    },
        onCancel() {
        	this.resetCuentaClienteForm();
        },
        aumentarCantidad() {
        	this.producto.cantidad = this.producto.cantidad + parseInt(1);
        },
        disminuirCantidad() {
        	if (this.producto.cantidad > 1) { this.producto.cantidad -= 1; }
        },
        resetCuentaClienteForm() {
        	this.cuenta_cliente.reset();
            this.producto.reset();
        	this.cuenta_cliente.errors.clear();
        	$('.select2').val('').trigger('change');
        },
        agregarItemACuenta() {
            this.producto.post('/agregar_item')
                .then(response => {
                    console.log(response);

                    let item = new Item({
                        id: response.id,
                        cantidad: response.cantidad,
                        producto: response.item.nombre,
                        precio: (response.item.precio * response.cantidad).toFixed(2)
                    });

                    this.factura.addItem(item);
                });
        },
        removerItemDeCuenta(item, index) {
            axios.post('/quitar_item', {
                id: item,
              })
              .then(function (response) {
              })
              .catch(function (error) {
                //console.log(error);
              });

            this.factura.removeItem(index);
        },
        facturarEImprimir() {

        }
    },
    computed: {
        neto() {
            return (this.factura.total / 1.18).toFixed(2);
        },
        impuestos() {
            return (this.factura.total * 0.18).toFixed(2);
        }
    }
});
