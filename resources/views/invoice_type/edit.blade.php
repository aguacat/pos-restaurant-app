{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Tipos de Facturas')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Tipos de Facturas
        <small>editar tipo factura</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mantenimientos</a></li>
        <li><a href="#">Tipos de Factura</a></li>
        <li class="active">Editando Tipos de Factura</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <div class="box box-purple">
                <div class="box-header">
                    <h3 class="box-title">Editando Tipo de Factura</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form id="form" method="post" action="{{ asset('/invoice_type/' . $current_item->id) }}"
                          role="form">
                        {{ csrf_field() }}

                        <div class="box-body">

                            @include('layouts.errors')

                            <div class="form-group">
                                <label for="invoice_type">Nombres</label>
                                <input type="text" class="form-control" id="invoice_type" name="invoice_type"
                                       placeholder="Tipo de Factura" value="{{ $current_item->invoice_type }}">
                            </div>

                            <div class="form-group">
                                <label for="last_name">Referencia</label>
                                <select class="form-control select2" name="reference" id="reference">
                                    <option></option>
                                    @foreach($references_list as $reference)
                                        <option value="{{ $reference->id }}"
                                                @if($current_item->id_reference == $reference->id) selected="selected" @endif>{{ $reference->description }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="has_tax"
                                           @if($current_item->has_tax) checked="checked" @endif> Requiere Impuesto
                                </label>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="rnc_required"
                                           @if($current_item->rnc_required) checked="checked" @endif> Requiere RNC
                                </label>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button id="patchForm" type="button" class="btn btn-black">Modificar</button>
                            <a href="{{ asset('/invoice_type') }}" class="btn btn-default">Cancelar</a>
                            <button id="destroyForm" type="button" class="btn btn-danger pull-right">Eliminar</button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-xs-8">

            @include('invoice_type.table')

        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    <script>
        //Manejando Formulario: update and delete
        var form = document.getElementById('form');
        var input = document.createElement("input");

        input.type = "hidden";
        input.name = "_method";

        form.onsubmit = function () {
            form.target = '_self';
        };

        document.getElementById('patchForm').onclick = function () {
            input.value = "PUT";
            form.appendChild(input);
            form.submit();
        };

        document.getElementById('destroyForm').onclick = function () {
            input.value = "DELETE";
            form.appendChild(input);
            form.submit();
        };

        $('.select2').select2({
            placeholder: "Elija una opción"
        });

        $('#form').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
    </script>

    @stack('table_scripts')
@stop