<div class="box box-black">
    <div class="box-header">
        <h3 class="box-title">Lista de Usuarios</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <table id="datatable" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th width="29%">Tipo de Factura</th>
                <th width="28%">Referencia</th>
                <th width="28%">Formato</th>
                <th class="text-center" width="7%">Tiene Impuesto</th>
                <th class="text-center" width="7%">Tiene RNC</th>
            </tr>
            </thead>
            @forelse($invoice_types_list as $invoice_type)
                <tr id="{{ $invoice_type->id }}"
                    @if(!empty($current_item) && $invoice_type->id == $current_item->id) class="black-mark" @endif>
                    <td>{{ $invoice_type->invoice_type }}</td>
                    <td>{{ $invoice_type->reference->description }}</td>
                    <td>{{ $invoice_type->reference->header }}</td>
                    <td class="text-center">
                        <input type="checkbox" name="has_tax" disabled="disabled"
                               @if($invoice_type->has_tax) checked="checked" @endif>
                    </td>
                    <td class="text-center">
                        <input type="checkbox" name="rnc_required" disabled="disabled"
                               @if($invoice_type->rnc_required) checked="checked" @endif>
                    </td>
                </tr>
            @empty
                <tr>
                    <td class="text-center" colspan="4">No hay tipos de facturas registradas.</td>
                </tr>
            @endforelse
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@push('table_scripts')
    <script>
        $(document).ready(function () {
            $("#datatable tbody").on('click', 'tr', function () {
                if ($(this).closest('tr').attr('id') != undefined) {
                    window.location = "{{ asset('/invoice_type') }}/" + $(this).closest('tr').attr('id') + '/edit';
                }
            });
        });
    </script>
@endpush