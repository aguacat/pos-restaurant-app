{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Tipos de Facturas')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Tipos de Facturas
        <small>lista completa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mantenimientos</a></li>
        <li><a href="#">Tipos de Factura</a></li>
        <li class="active">Lista Tipos de Factura</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <div class="box box-black">
                <div class="box-header">
                    <h3 class="box-title">Nuevo Tipo de Factura</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form method="post" action="{{ asset('/invoice_type') }}" role="form">
                        {{ csrf_field() }}

                        <div class="box-body">

                            @include('layouts.errors')

                            <div class="form-group">
                                <label for="invoice_type">Nombres</label>
                                <input type="text" class="form-control" id="invoice_type" name="invoice_type"
                                       placeholder="Tipo de Factura" value="{{ old('invoice_type') }}">
                            </div>

                            <div class="form-group">
                                <label for="last_name">Referencia</label>
                                <select class="form-control select2" name="reference" id="reference">
                                    <option></option>
                                    @foreach($references_list as $reference)
                                        <option value="{{ $reference->id }}">{{ $reference->description }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="has_tax" checked="checked"> Requiere Impuesto
                                </label>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="rnc_required" checked="checked"> Requiere RNC
                                </label>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-black">Guardar</button>
                            <button type="reset" class="btn btn-cancel pull-right">Cancelar</button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-xs-8">

            @include('invoice_type.table')

        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    {{--    <script src="{{ asset('/js/jquery.blockUI.js') }}"></script>--}}

    <script>
        $('.select2').select2({
            placeholder: "Elija una opción"
        });
    </script>

    @stack('table_scripts')
@stop