<div class="col-md-12">
    <div class="box box-solid">
        @if(!empty($current_item))
            <div class="box-header with-border">
                <form method="post" class="form-inline pull-right"
                      action="{{ asset('/order/' . $current_item->id . '/addAccount') }}">

                    {{ csrf_field() }}

                    <div class="form-group">
                        <input type="text" class="form-control" id="account_name" name="account_name"
                               placeholder="Nombre de Cuenta" onkeyup="validateAccountName($(this))">
                    </div>
                    <button id="AddAccountButton" type="submit" class="btn btn-black" disabled>Agregar Cuenta</button>
                </form>
            </div>
    @endif
    <!-- /.box-header -->
        <div class="box-body">
            @if(empty($current_item))
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>Elija Una Mesa</h3>
                        <p>para registrar órdenes</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                </div>
            @else
                <div class="box-group" id="accordion">
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    @forelse($open_accounts as $account)
                        <div class="panel box box-black">
                            <div class="box-header with-border">
                                @forelse($table_orders as $order)
                                    @break(($order->id == $account->id))

                                    @if(($loop->last && $order->id != $account->id))
                                        {{--<div class="pull-right">--}}
                                        {{--<button id="{{ 'closeAccount_' . $account->id }}"--}}
                                        {{--class="btn btn-xs btn-danger pull-left"--}}
                                        {{--onclick="closeAccount($(this))">Cerrar Cuenta--}}
                                        {{--</button>--}}
                                        {{--</div>--}}
                                        @break
                                    @endif

                                @empty
                                    {{--<div class="pull-right">--}}
                                    {{--<button id="{{ 'closeAccount_' . $account->id }}"--}}
                                    {{--class="btn btn-xs btn-danger pull-left" onclick="closeAccount($(this))">--}}
                                    {{--Cerrar Cuenta--}}
                                    {{--</button>--}}
                                    {{--</div>--}}
                                @endforelse
                                <h4 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#{{ $account->id }}"
                                       aria-expanded="false"
                                       class="collapsed">
                                        <span class="black" style="color: #222d32">Cuenta: {{ $account->name }}</span>
                                    </a>
                                </h4>
                            </div>

                            <div id="{{ $account->id }}" class="panel-collapse collapse" style="height: 0px;">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <form class="form" method="post"
                                                action="{{ asset('/order/' . $account->id_table . '/accountDetail/' . $account->id) }}">
                                                
                                                {{ csrf_field() }}

                                                @include('layouts.errors')

                                                <input type="hidden" name="id_account" id="id_account" value="{{ $account->id }}">

                                                <div class="form-group">
                                                    <label for="quantity">Cantidad:</label>
                                                    <input type="text" 
                                                            class="form-control" 
                                                            name="quantity" 
                                                            id="quantity" 
                                                            value="{{ old('quantity', 1) }}"
                                                            placeholder="Cantidad (Max. 20)" 
                                                            autocomplete="off">
                                                </div>

                                                <div class="form-group">
                                                    <label for="food">Elegir del Menú:</label>
                                                    <select class="form-control select2" name="food" id="food">
                                                        <option></option>
                                                        @foreach($food_list as $food)
                                                            <option value="{{ $food->id }}" @if(old('food') == $food->id) selected="selected" @endif>
                                                                {{ $food->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="food_comment">Notas de la Orden:</label>
                                                    <input type="text" class="form-control" placeholder="Notas de la Orden" name="food_comment" id="food_comment" value="{{ old('food_comment') }}">
                                                </div>

                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-black">Agregar</button>
                                                    <a href="{{ asset('/order/'. $account->id . '/closeAccount') }}" class="btn btn-danger pull-right">Cerrar</a>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-8">
                                            <table class="table table-condensed">
                                                    <thead>
                                                    <tr>
                                                        <th with="50%">Producto</th>
                                                        <th class="text-center" width="25%">Estado Orden</th>
                                                        <th class="text-center" width="25%">Cancelar</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
            
                                                    @php $orderItem = 0; @endphp
            
                                                    @forelse($table_orders as $order)
                                                        @continue($order->id != $account->id)
                                                        <tr>
                                                            <td>{{ $order->food }}</td>
                                                            <td class="text-center">{{ $order->order_status }}</td>
                                                            @if(($order->id_order_status == 1 || $order->id_order_status == 7))
                                                                <td class="text-center">
                                                                    <button id="{{ $order->id_order }}" type="button"
                                                                            class="btn btn-xs btn-danger"
                                                                            onclick="cancelOrder($(this))">Cancelar Orden
                                                                    </button>
                                                                </td>
                                                            @else
                                                                <td class="text-center">No Cancelable</td>
                                                            @endif
                                                        </tr>
            
                                                        @php $orderItem = 1; @endphp
            
                                                    @empty
                                                        <tr colspan="3">Esta cuenta no tiene órdenes pendientes.</tr>
                                                    @endforelse
                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <h3 class="text-center">No hay cuentas abiertas</h3>
                    @endforelse

                </div>
            @endif
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>

@push('detail_scripts')
    <script>
        var $showAccount = '{{ !empty($showAccount) ? $showAccount : false }}'

        function validateAccountName(element) {
            if (element.val().length > 2) {
                $('#AddAccountButton').prop('disabled', false);
            } else {
                $('#AddAccountButton').prop('disabled', true);
            }
        }

        function cancelOrder(element) {
            var orderId = element.prop('id');
            var orderTableData = '';
            var orderAccountData = '';

            $.put('{{ asset('/order') }}/' + orderId + '/cancelOrder',
                {
                    _token: "{{ csrf_token() }}",
                    _method: "PUT"
                })
                .done(function (data) {
                    orderTableData = data.id_table;
                    orderAccountData = data.id_account;
                    location.reload('{{ asset('/order') }}/' + orderTableData + '/accountDetail/' + orderAccountData);
                });
        }

        function closeAccount(element) {
            var accountId = element.prop('id').replace('closeAccount_', '');
            var orderTableData = '';

            $.put('{{ asset('/order') }}/' + accountId + '/closeAccount',
                {
                    _token: "{{ csrf_token() }}",
                    _method: "PUT"
                })
                .done(function (data) {
                    orderTableData = data.id_table;
                    orderAccountData = data.id_account;
                    location.reload('{{ asset('/order') }}/' + orderTableData + '/orderbytable');
                });
        }

        $(document).ready(function () {
            $("#datatable tbody").on('click', 'tr', function () {
                if ($(this).closest('tr').attr('id') != undefined) {
                    window.location = "{{ asset('/food_type') }}/" + $(this).closest('tr').attr('id') + '/edit';
                }
            });

            if ($showAccount) {
                $('#' + $showAccount).collapse('show');
            }
        });
    </script>
@endpush