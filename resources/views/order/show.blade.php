{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Órdenes')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Órdenes
        <small>lista completa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Facturación</a></li>
        <li><a href="#"> Órdenes</a></li>
        <li class="active">Mesas del Restaurante</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            @forelse($tables as $table)
                <div class="small-box @if(!empty($current_item) && $current_item->id == $table->id) bg-green @else bg-aqua @endif">
                    <div class="inner">
                        <h3>{{ $table->name }}</h3>
                        <p>Status: Abierta/Cerrada</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-glass"></i>
                    </div>
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked">
                            <li><a href="{{ asset('/order/' . $table->id . '/orderbytable') }}">Seleccionar Mesa <span
                                            class="pull-right badge bg-blue">Cuentas: {{ $accounts_number[$loop->index]->Cuentas }}</span></a>
                            </li>
                            <li><a href="#">Ver Órdenes Pendientes<span class="pull-right badge bg-yellow">Pendientes:
                                        0</span></a></li>
                        </ul>
                    </div>
                </div>
            @empty
                <p>No hay mesas registradas.</p>
            @endforelse
        </div>
        <div class="col-xs-8">

            @include('order.detail')

        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    <script src="{{ asset('/js/custom.js') }}"></script>
    <script src="{{ asset('/js/jquery.blockUI.js') }}"></script>

    <script>
        $('.select2').select2({
            width: "100%",
            placeholder: "Elija una opción"
        });
    </script>

    @stack('detail_scripts')
@stop