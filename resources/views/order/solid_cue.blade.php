{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Órdenes Comida')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Órdenes
        <small>lista completa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Facturación</a></li>
        <li><a href="#"> Órdenes</a></li>
        <li class="active">Mesas del Restaurante</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <h3 class="text-center">Pendientes</h3>
            @foreach($pending_food_list as $food)
                @continue(in_array($food->cue_type, array('B')))
                <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header card-background bg-green-active">
                        <h3 class="widget-user-username">{{ $food->food }}</h3>
                        <h5 class="widget-user-desc"><b>NOTA:</b> {{ $food->food_comment OR '-' }}</h5>
                    </div>
                    <div class="widget-user-image">
                        <img class="img-circle" src="{{ $food->image }}" alt="Comida">
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header timer">
                                        <span class="hidden initial_time">{{ $food->created_at }}</span>
                                        <span class="timer_data">0</span>
                                    </h5>
                                    <span class="description-text">Tiempo</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">{{ $food->account }}</h5>
                                    <span class="description-text">{{ $food->table_name }}</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div class="description-block">
                                    <span class="description-text">
                                        <button id="{{ $food->id }}" type="button" class="btn btn-md btn-black"
                                                onclick="moveToNextStep($(this), 'en_proceso')">Preparando
                                        </button>
                                        &nbsp
                                    </span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
            @endforeach
        </div>

        <div class="col-xs-4">
            <h3 class="text-center">En Proceso</h3>
            @foreach($preparing_food_list as $food)
                @continue(in_array($food->cue_type, array('B')))
                <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header card-background bg-green-active">
                        <h3 class="widget-user-username">{{ $food->food }}</h3>
                        <h5 class="widget-user-desc"><b>NOTA:</b> {{ $food->food_comment OR '-' }}</h5>
                    </div>
                    <div class="widget-user-image">
                        <img class="img-circle" src="{{ $food->image }}" alt="Comida">
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header timer">
                                        <span class="hidden initial_time">{{ $food->created_at }}</span>
                                        <span class="timer_data">0</span>
                                    </h5>
                                    <span class="description-text">Tiempo</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">{{ $food->account }}</h5>
                                    <span class="description-text">{{ $food->table_name }}</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div class="description-block">
                                    <span class="description-text">
                                        <button id="{{ $food->id }}" type="button" class="btn btn-md btn-black"
                                                onclick="moveToNextStep($(this), 'lista')">Lista
                                        </button>
                                        &nbsp
                                    </span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
            @endforeach
        </div>
        <div class="col-xs-4">
            <h3 class="text-center">Listas</h3>
            @foreach($ready_food_list as $food)
                @continue(in_array($food->cue_type, array('B')))
                <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header card-background bg-green-active">
                        <h3 class="widget-user-username">{{ $food->food }}</h3>
                        <h5 class="widget-user-desc"><b>NOTA:</b> {{ $food->food_comment OR '-' }}</h5>
                    </div>
                    <div class="widget-user-image">
                        <img class="img-circle" src="{{ $food->image }}" alt="Comida">
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header timer">
                                        <span class="hidden initial_time">{{ $food->created_at }}</span>
                                        <span class="timer_data">0</span>
                                    </h5>
                                    <span class="description-text">Tiempo</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">{{ $food->account }}</h5>
                                    <span class="description-text">{{ $food->table_name }}</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div class="description-block">
                                    <span class="description-text">
                                        <button id="{{ $food->id }}" type="button" class="btn btn-md btn-black"
                                                onclick="moveToNextStep($(this), 'entregada')">Entregada
                                        </button>
                                        &nbsp
                                    </span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    <script src="{{ asset('/js/moment.js') }}"></script>

    <script>
        window.setInterval(elapsedTime, 1);
        //window.setInterval(refreshPage, 5000);

        function refreshPage() {
            window.location = '{{ asset('/order_cue/solidCue') }}';
        }

        function elapsedTime() {
            $('.timer').each(function () {
                var start = $(this).find('span.initial_time').text();
                var startConverted = moment.utc(start);

                var elapsedTime = moment.utc(moment.utc().diff(startConverted)).format('HH:mm:ss');

                var seconds = moment.duration(elapsedTime);
                $(this).find('span.timer_data').text(elapsedTime);

                if (seconds._milliseconds > 300000) {
                    $(this).closest('.box-widget').find('div.card-background ').removeClass('bg-green-active').addClass('bg-orange-active');
                }

                if (seconds._milliseconds > 600000) {
                    $(this).closest('.box-widget').find('div.card-background ').removeClass('bg-orange-active').addClass('bg-red-active');
                }
            });
        }

        function moveToNextStep(element, status) {
            var orderId = element.prop('id');

            $.post('{{ asset('/order/') }}/' + orderId + '/cueStatus/' + status,
                {
                    _token: "{{ csrf_token() }}",
                })
                .done(function (data) {
                    window.location = '{{ asset('/order_cue/solidCue') }}';
                });
        }

        $('.select2').select2({
            width: 'resolve',
            placeholder: "Elija una opción"
        });
    </script>

    @stack('detail_scripts')
@stop

