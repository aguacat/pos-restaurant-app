@if(count($errors))
<div id="validation-errors" class="form-group">
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul id="form-errors">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<div id="js-notification-block" class="form-group hidden">
    <div id="js-notification-type" class="alert alert-danger">
        <ul id="js-notification-detail">
        </ul>
    </div>
</div>