<div class="box box-black">
    <div class="box-header">
        <h3 class="box-title">Lista de Usuarios</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <table id="datatable" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th width="25%">Nombres</th>
                <th width="25%">Apellidos</th>
                <th width="25%">Correo Electrónico</th>
                <th width="25%">Tipo de Usuario</th>
            </tr>
            </thead>
            @forelse($user_list as $user)
                <tr id="{{ $user->id }}"
                    @if(!empty($current_item) && $user->id == $current_item->id) class="black-mark" @endif>
                    <td>{{ $user->first_name }}</td>
                    <td>{{ $user->last_name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->user_type->user_type }}</td>
                </tr>
            @empty
                <tr>
                    <td class="text-center" colspan="4">No hay usuarios registrados.</td>
                </tr>
            @endforelse
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@push('table_scripts')
    <script>
        $(document).ready(function () {
            $("#datatable tbody").on('click', 'tr', function () {
                if ($(this).closest('tr').attr('id') != undefined) {
                    window.location = "{{ asset('/user') }}/" + $(this).closest('tr').attr('id') + '/edit';
                }
            });
        });
    </script>
@endpush