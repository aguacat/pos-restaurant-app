{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Edición Usuario')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Usuarios
        <small>editar usuario</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mantenimientos</a></li>
        <li><a href="#">Usuarios</a></li>
        <li class="active">Editar Usuario</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <div class="box box-purple">
                <div class="box-header">
                    <h3 class="box-title">Editando Usuario</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form id="form" method="post" action="{{ asset('/user/' . $current_item->id) }}" role="form">
                        {{ csrf_field() }}

                        <div class="box-body">

                            @include('layouts.errors')

                            <div class="form-group">
                                <label for="first_name">Nombres</label>
                                <input type="text" class="form-control" id="first_name" name="first_name"
                                       placeholder="Nombres" value="{{ $current_item->first_name }}">
                            </div>

                            <div class="form-group">
                                <label for="last_name">Apellidos</label>
                                <input type="text" class="form-control" id="last_name" name="last_name"
                                       placeholder="Apellidos" value="{{ $current_item->last_name }}">
                            </div>

                            <div class="form-group">
                                <label for="email">Correo Electrónico</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       placeholder="Correo Electrónico" value="{{ $current_item->email }}">
                            </div>

                            <div class="form-group">
                                <label for="password">Contraseña</label>
                                <input type="password" class="form-control" id="password" name="password"
                                       placeholder="Contraseña">
                            </div>

                            <div class="form-group">
                                <label for="password_confirmation">Confirmar Contraseña</label>
                                <input type="password" class="form-control" id="password_confirmation"
                                       name="password_confirmation"
                                       placeholder="Confirmar Contraseña">
                            </div>

                            <div class="form-group">
                                <label for="user_type">Tipo de Usuaro</label>
                                <select class="form-control select2" name="user_type" id="user_type">
                                    <option></option>
                                    @foreach($user_type_list as $user_type)
                                        <option value="{{ $user_type->id }}"
                                                @if($current_item->id_user_type == $user_type->id) selected="selected" @endif>{{ $user_type->user_type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button id="patchForm" type="button" class="btn btn-black">Modificar</button>
                            <a href="{{ asset('/user_type') }}" class="btn btn-default">Cancelar</a>
                            <button id="destroyForm" type="button" class="btn btn-danger pull-right">Eliminar</button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-xs-8">

            @include('user.table')

        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    <script>
        //Manejando Formulario: update and delete
        var form = document.getElementById('form');
        var input = document.createElement("input");

        input.type = "hidden";
        input.name = "_method";

        form.onsubmit = function () {
            form.target = '_self';
        };

        document.getElementById('patchForm').onclick = function () {
            input.value = "PUT";
            form.appendChild(input);
            form.submit();
        };

        document.getElementById('destroyForm').onclick = function () {
            input.value = "DELETE";
            form.appendChild(input);
            form.submit();
        };

        $('.select2').select2({
            placeholder: "Elija una opción"
        });
    </script>

    @stack('table_scripts')
@stop