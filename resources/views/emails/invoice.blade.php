@component('mail::message', ['invoice' => $invoice])
![product](http://www.brunchcafe.store/images/brunch_logo.jpg)

Estimado Cliente, <br>
Aquí el detalle de su factura:

**Nombre: ** {{ $invoice['nombre'] }} <br>
**Fecha: ** {{ $invoice['date'] }} <br>
**Tipo Factura: ** {{ $invoice['type'] }} <br>
**RNC: ** {{ $invoice['RNC'] }} <br>
**NCF: ** {{ $invoice['NCF'] }} <br>


@component('mail::table')
| Cantidad       | Producto         | Precio  |
| :-------------: |:-------------:| --------:|
@foreach($invoice['invoice_detail'] as $detail)
    | 01 | {{ $detail['food'] }} | {{ $detail['price'] }} |
@endforeach
@endcomponent

@component('mail::table')
| |  |
| -------------:| --------:|
| Subtotal:     | **{{ $invoice['sub_total'] }}**     |
| Impuestos (18%):     | **{{ $invoice['taxes'] }} **     |
| Impuesto de Ley (10%): | **{{ $invoice['taxes_10'] }}** |
| Total:    |  **{{ $invoice['total'] }}**  |
| |  |
| Total Pagado: | **{{ $invoice['amount_paid'] }}**  |
| Total Devuelto:    | **{{ $invoice['amount_to_return'] }}**  |
@endcomponent

<p style="text-align: center">¡Gracias por preferirnos!</p> <br/>

@component('mail::panel')
<p style="text-align: center">"La salvación es del Señor. <br>¡Sea sobre tu pueblo tu bendición!" <br> Salmo 3:8
</p>
@endcomponent

@endcomponent