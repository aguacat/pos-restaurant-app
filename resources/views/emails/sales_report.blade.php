@component('mail::message', ['invoice' => $invoice])
![product](http://www.brunchcafe.store/images/brunch_logo.jpg)

Estimad@s, <br>
Se ha generado el reporte de ventas del mes de {{ $report['month'] }}:

@component('mail::table')
@foreach($report['totals'] as $sales)
| **Tipo Comprobante:** | {{ $sales->NCF }} | 
| :------- | -------: |
| **Cantidad:** | {{ $sales->conteo }} | 
| **Sub-Total:** | {{ $sales->sub_total }} |
| **ITIBIS:** | {{ $sales->taxes }} | 
| **10%:** | {{ $sales->taxes_10 }} |
| **Total:** | {{ $sales->total }} |
@endforeach
@endcomponent

@component('mail::button', ['url' => $report['url']])
Ver Reporte
@endcomponent

<p style="text-align: center">¡Gracias por preferirnos!</p> <br/>

@component('mail::panel')
<p style="text-align: center">"La salvación es del Señor. <br>¡Sea sobre tu pueblo tu bendición!" <br> Salmo 3:8</p>
@endcomponent

@endcomponent
