<div class="box box-black">
    <div class="box-header">
        <h3 class="box-title">Lista de Tipos de Usuario</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <table id="datatable" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th width="100%">Tipo de Usuario</th>
            </tr>
            </thead>
            @forelse($user_type_list as $user_type)
                <tr id="{{ $user_type->id }}"
                    @if(!empty($current_item) && $user_type->id == $current_item->id) class="black-mark" @endif>
                    <td>{{ $user_type->user_type }}</td>
                </tr>
            @empty
                <tr>
                    <td class="text-center" colspan="2">No hay tipos de usuarios registrados.</td>
                </tr>
            @endforelse
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@push('table_scripts')
    <script>
        $(document).ready(function () {
            $("#datatable tbody").on('click', 'tr', function () {
                if ($(this).closest('tr').attr('id') != undefined) {
                    window.location = "{{ asset('/user_type') }}/" + $(this).closest('tr').attr('id') + '/edit';
                }
            });
        });
    </script>
@endpush