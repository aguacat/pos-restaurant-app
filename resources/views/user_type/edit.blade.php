{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Edición Tipo de Usuario')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Tipos de Usuario
        <small>editar tipo de usuario</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mantenimientos</a></li>
        <li><a href="#">Tipos de Usuarios</a></li>
        <li class="active">Editar Tipo Usuario</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <div class="box box-purple">
                <div class="box-header">
                    <h3 class="box-title">Editando Tipo de Usuario</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form id="form" method="post" action="{{ asset('/user_type/' . $current_item->id) }}" role="form">
                        {{ csrf_field() }}

                        <div class="box-body">

                            @include('layouts.errors')

                            <div class="form-group">
                                <label for="user_type">Tipo de Usuario</label>
                                <input type="text" class="form-control" id="user_type" name="user_type"
                                       placeholder="Tipo de Usuario" value="{{ $current_item->user_type }}">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button id="patchForm" type="button" class="btn btn-black">Modificar</button>
                            <a href="{{ asset('/user_type') }}" class="btn btn-default">Cancelar</a>
                            <button id="destroyForm" type="button" class="btn btn-danger pull-right">Eliminar</button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-xs-8">

            @include('user_type.table')

        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    <script>
        //Manejando Formulario: update and delete
        var form = document.getElementById('form');
        var input = document.createElement("input");

        input.type = "hidden";
        input.name = "_method";

        form.onsubmit = function () {
            form.target = '_self';
        };

        document.getElementById('patchForm').onclick = function () {
            input.value = "PUT";
            form.appendChild(input);
            form.submit();
        };

        document.getElementById('destroyForm').onclick = function () {
            input.value = "DELETE";
            form.appendChild(input);
            form.submit();
        };

        $('.select2').select2({
            placeholder: "Elija una opción"
        });
    </script>

    @stack('table_scripts')
@stop