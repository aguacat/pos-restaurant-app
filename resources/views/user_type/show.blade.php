{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Tipos de Usuario')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Tipos de Usuario
        <small>lista completa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mantenimientos</a></li>
        <li><a href="#">Tipos de Usuario</a></li>
        <li class="active">Lista Tipos de Usuario</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <div class="box box-black">
                <div class="box-header">
                    <h3 class="box-title">Nuevo Tipo de Usuario</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form method="post" action="{{ asset('/user_type') }}" role="form">
                        {{ csrf_field() }}

                        <div class="box-body">

                            @include('layouts.errors')

                            <div class="form-group">
                                <label for="user_type">Tipo de Usuario</label>
                                <input type="text" class="form-control" id="user_type" name="user_type"
                                       placeholder="Tipo de Usuario" value="{{ old('user_type') }}">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-black">Guardar</button>
                            <button type="reset" class="btn btn-cancel pull-right">Cancelar</button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-xs-8">

            @include('user_type.table')

        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    {{--    <script src="{{ asset('/js/jquery.blockUI.js') }}"></script>--}}

    <script>
        $('.select2').select2({
            placeholder: "Elija una opción"
        });
    </script>

    @stack('table_scripts')
@stop