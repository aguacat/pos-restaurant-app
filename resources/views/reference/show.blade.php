{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Referencias')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Referencias
        <small>lista completa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mantenimientos</a></li>
        <li><a href="#">Referencias</a></li>
        <li class="active">Lista Referencias</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <div class="box box-black">
                <div class="box-header">
                    <h3 class="box-title">Nueva Referencia</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form method="post" action="{{ asset('/reference') }}" role="form">
                        {{ csrf_field() }}

                        <div class="box-body">

                            @include('layouts.errors')

                            <div class="form-group">
                                <label for="header">Encabezado</label>
                                <input type="text" class="form-control" id="header" name="header"
                                       placeholder="Encabezado" value="{{ old('header') }}">
                            </div>

                            <div class="form-group">
                                <label for="description">Descripción</label>
                                <input type="text" class="form-control" id="description" name="description"
                                       placeholder="Descripción" value="{{ old('description') }}">
                            </div>

                            <div class="form-group">
                                <label for="birthdate">Fecha Vencimiento</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="fecha_vencimiento" name="fecha_vencimiento">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="start">Inicio</label>
                                <input type="text" class="form-control" id="start" name="start"
                                       placeholder="Inicio" value="{{ old('start') }}">
                            </div>

                            <div class="form-group">
                                <label for="end">Fin</label>
                                <input type="text" class="form-control" id="end" name="end"
                                       placeholder="Fin" value="{{ old('end') }}">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-black">Guardar</button>
                            <button type="reset" class="btn btn-cancel pull-right">Cancelar</button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-xs-8">

            @include('reference.table')

        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    <script src="{{ asset('/js/bootstrap-datepicker.min.js') }}"></script>
    {{--    <script src="{{ asset('/js/jquery.blockUI.js') }}"></script>--}}

    <script>
        $('#fecha_vencimiento').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        });

        $('.select2').select2({
            placeholder: "Elija una opción"
        });
    </script>

    @stack('table_scripts')
@stop
