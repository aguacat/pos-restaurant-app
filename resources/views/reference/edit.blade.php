{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Referencias')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Referencias
        <small>editar referencia</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mantenimientos</a></li>
        <li><a href="#">Referencia</a></li>
        <li class="active">Editar Referencia</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <div class="box box-purple">
                <div class="box-header">
                    <h3 class="box-title">Editando Referencia</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form id="form" method="post" action="{{ asset('/reference/' . $current_item->id) }}" role="form">
                        {{ csrf_field() }}

                        <div class="box-body">

                            @include('layouts.errors')

                            <div class="form-group">
                                <label for="header">Encabezado</label>
                                <input type="text" class="form-control" id="header" name="header"
                                       placeholder="Encabezado" value="{{ $current_item->header }}">
                            </div>

                            <div class="form-group">
                                <label for="description">Descripción</label>
                                <input type="text" class="form-control" id="description" name="description"
                                       placeholder="Descripción" value="{{ $current_item->description }}">
                            </div>

                            <div class="form-group">
                                <label for="fecha_vencimiento">Fecha Vencimiento</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="fecha_vencimiento" name="fecha_vencimiento"
                                           value="{{ $current_item->fecha_vencimiento }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="start">Inicio</label>
                                <input type="text" class="form-control" id="start" name="start"
                                       placeholder="Inicio" value="{{ $current_item->start }}">
                            </div>

                            <div class="form-group">
                                <label for="end">Fin</label>
                                <input type="text" class="form-control" id="end" name="end"
                                       placeholder="Fin" value="{{ $current_item->end }}">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button id="patchForm" type="button" class="btn btn-black">Modificar</button>
                            <a href="{{ asset('/reference') }}" class="btn btn-default">Cancelar</a>
                            <button id="destroyForm" type="button" class="btn btn-danger pull-right">Eliminar</button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-xs-8">

            @include('reference.table')

        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    <script src="{{ asset('/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        //Manejando Formulario: update and delete
        var form = document.getElementById('form');
        var input = document.createElement("input");

        input.type = "hidden";
        input.name = "_method";

        form.onsubmit = function () {
            form.target = '_self';
        };

        document.getElementById('patchForm').onclick = function () {
            input.value = "PUT";
            form.appendChild(input);
            form.submit();
        };

        document.getElementById('destroyForm').onclick = function () {
            input.value = "DELETE";
            form.appendChild(input);
            form.submit();
        };

        $('#fecha_vencimiento').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        });

        $('.select2').select2({
            placeholder: "Elija una opción"
        });
    </script>

    @stack('table_scripts')
@stop
