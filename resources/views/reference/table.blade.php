<div class="box box-black">
    <div class="box-header">
        <h3 class="box-title">Lista de Referencias</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <table id="datatable" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th width="20%">Encabezado</th>
                <th width="34%">Descripción</th>
                <th class="text-center" width="16%">Vence En:</th>
                <th class="text-center" width="10%">Inicio</th>
                <th class="text-center" width="10%">Fin</th>
                <th width="10%">Secuencia</th>
            </tr>
            </thead>
            @forelse($reference_list as $reference)
                <tr id="{{ $reference->id }}"
                    @if(!empty($current_item) && $reference->id == $current_item->id) class="black-mark" @endif>
                    <td>{{ $reference->header }}</td>
                    <td>{{ $reference->description }}</td>
                    <td>{{ $reference->fecha_vencimiento }}</td>
                    <td>{{ $reference->start }}</td>
                    <td>{{ $reference->end }}</td>
                    <td>{{ $reference->sequence }}</td>
                </tr>
            @empty
                <tr>
                    <td class="text-center" colspan="5">No hay referencias registradas.</td>
                </tr>
            @endforelse
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@push('table_scripts')
    <script>
        $(document).ready(function () {
            $("#datatable tbody").on('click', 'tr', function () {
                if ($(this).closest('tr').attr('id') != undefined) {
                    window.location = "{{ asset('/reference') }}/" + $(this).closest('tr').attr('id') + '/edit';
                }
            });
        });
    </script>
@endpush
