{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Facturas')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Facturas
        <small>facturación</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Restaurante</a></li>
        <li><a href="#">Facturación</a></li>
        <li class="active">Factura</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <div class="box box-black">
                <div class="box-header">
                    <h3 class="box-title">Cuentas a Facturar</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form method="post" role="form" onsubmit="return false">
                        {{ csrf_field() }}

                        <div class="box-body">

                            @include('layouts.errors')

                            <div class="form-group">
                                <label for="account">Nombre:</label>
                                <select class="form-control select2" name="invoice" id="invoice"
                                        onchange="loadInvoiceDetail(this)">
                                    <option></option>
                                    @foreach($invoice_list as $invoice)
                                        <option value="{{ $invoice->id }}">{{ $invoice->nombre . ' | ' . $invoice->created_at }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label for="invoice_name">Correo Electrónico:</label>
                                <input type="text" class="form-control" id="invoice_mail" name="invoice_mail"
                                       placeholder="Correo del Cliente" disabled="disabled">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button id="send_by_mail_btn" type="button" class="btn btn-black" onclick="sendByEmail()"
                                    disabled="disabled"><i class="fa fa-envelope"></i></button>
                            <button id="print_invoice_btn" type="button" class="btn btn-black"
                                    onclick="printInvoice()" disabled="disabled"><i class="fa fa-print"></i>
                            </button>
                            <a href="{{ asset('/print') }}" class="btn btn-default pull-right">Cancelar</a>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-xs-8">

            @include('print.detail')

        </div>
    </div>

    <div class="modal fade" id="printing-modal" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Información del Sistema</h4>
                </div>
                <div class="modal-body">
                    <p id="modal-message">Re-imprimiendo.</p>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="mailed-modal" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Información del Sistema</h4>
                </div>
                <div class="modal-body">
                    <p id="modal-message">Factura Enviada.</p>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    {{--    <script src="{{ asset('/js/jquery.blockUI.js') }}"></script>--}}
    <script src="{{ asset('/js/custom.js') }}"></script>

    <script>
        function loadInvoiceDetail(element) {
            var id = element.options[element.selectedIndex].value;

            $.get('{{ asset('/print') }}/' + id)
                .done(function (data) {
                    document.getElementById('invoice_mail').disabled = false;
                    document.getElementById('print_invoice_btn').disabled = false;

                    document.getElementById('invoice_name').innerText = data.invoice.nombre;
                    document.getElementById('invoice_date').innerText = data.invoice.date;
                    document.getElementById('invoice_type').innerText = data.invoice_type;
                    document.getElementById('invoice_rnc').innerText = data.invoice.RNC;
                    document.getElementById('invoice_nfc').innerText = data.invoice.NCF;
                    document.getElementById('sub_total').innerText = data.invoice.sub_total;
                    document.getElementById('taxes').innerText = data.invoice.taxes;
                    document.getElementById('taxes_10').innerText = data.invoice.taxes_10;
                    document.getElementById('total').innerText = data.invoice.total;

                    $('#orderDetail').empty();

                    $.each(data.details, function (index, value) {
                        var columna1 = '<td class="text-center"> 01 </td>';
                        var columna2 = '<td class="item_name">' + value.food + '</td>';
                        var columna3 = '<td class="text-right item_price">' + value.price + '</td>';
                        var fila = '<tr id="' + value.id + '">' + columna1 + columna2 + columna3 + '</tr>';

                        $('#orderDetail').append(fila);
                    });
                });
        }

        function sendByEmail() {
            var id = document.getElementById('invoice').options[document.getElementById('invoice').selectedIndex].value;
            document.getElementById('send_by_mail_btn').disabled = true;

            $.post('{{ asset('/print') }}/' + id + '/mail', {
                _token: "{{ csrf_token() }}",
                invoice_mail: document.getElementById('invoice_mail').value
            }).done(function (data) {
                console.log(data);
                if (data) {
                    $('#mailed-modal').modal('show');

                    document.getElementById('send_by_mail_btn').disabled = true;
                    document.getElementById('print_invoice_btn').disabled = true;

                    $('#orderDetail').empty();

                    document.getElementById('invoice_name').innerText = '';
                    document.getElementById('invoice_date').innerText = '';
                    document.getElementById('invoice_type').innerText = '';
                    document.getElementById('invoice_rnc').innerText = '';
                    document.getElementById('invoice_nfc').innerText = '';
                    document.getElementById('sub_total').innerText = '0.00';
                    document.getElementById('taxes').innerText = '0.00';
                    document.getElementById('taxes_10').innerText = '0.00';
                    document.getElementById('total').innerText = '0.00';

                    document.getElementById('invoice_mail').value = '';

                }
            })
        }

        function printInvoice() {
            var id = document.getElementById('invoice').options[document.getElementById('invoice').selectedIndex].value;

            $.post('{{ asset('/print') }}/' + id + '/print', {
                _token: "{{ csrf_token() }}"
            })
                .done(function (data) {
                    console.log(data);
                    $('#printing-modal').modal('show');

                    var items = [];

                    $('#orderDetail tr').each(function (item, value) {
                        var item_name = $(value).find('.item_name').html();
                        var item_price = $(value).find('.item_price').html();

                        var item = {};
                        item.item_name = item_name;
                        item.item_price = item_price;

                        items.push(item);
                    });

                    $.post('https://localhost/print/invoice.php', {
                        cashier_name: '{{ auth()->guard('web')->user()->first_name . ' ' . auth()->guard('web')->user()->last_name }}',
                        invoice_name: data.invoice.nombre,
                        invoice_type: data.invoice_type,
                        sub_total: data.invoice.sub_total,
                        taxes: data.invoice.taxes,
                        taxes_10: data.invoice.taxes_10,
                        total: data.invoice.total,
                        rnc: data.invoice.RNC,
                        ncf: data.invoice.NCF,
                        items: JSON.stringify(items)
                    })
                        .done(function (data) {
                            $('#printing-modal').modal('hide');
                            window.location = '{{ asset('/print') }}';
                        });                    
                });
        }

        $('#invoice_mail').on('keyup', function () {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            if (re.test(document.getElementById('invoice_mail').value)) {
                document.getElementById('send_by_mail_btn').disabled = false;
            } else {
                document.getElementById('send_by_mail_btn').disabled = true;
            }
        });

        $('.select2').select2({
            placeholder: "Elija una opción",
            readonly: true
        });
    </script>

    @stack('table_scripts')
@stop
