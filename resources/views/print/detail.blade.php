<div class="box box-black">
    <div class="box-header">
        <h3 class="box-title">Detalle de Factura</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="row">

            <!-- Main content -->
            <section class="invoice">
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-5 invoice-col">
                        <address>
                            <strong>Nombre: </strong><span id="invoice_name"></span><br>
                            <strong>Fecha: </strong><span id="invoice_date"></span><br>
                            <strong>Tpo Factura: </strong><span id="invoice_type"></span><br>
                            <strong>RNC: </strong><span id="invoice_rnc"></span><br>
                            <strong>NCF: </strong><span id="invoice_nfc"></span><br>
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col"></div>
                    <div class="col-sm-3 invoice-col">
                        <img class="img-responsive" src="{{ asset('/images/brunch_logo.jpg') }}" alt="Logo">
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- Table row -->
                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="text-center" width="10%">Cantidad</th>
                                <th class="text-left" width="75%">Producto</th>
                                <th class="text-right" width="15%">Sub-Total</th>
                            </tr>
                            </thead>
                            <tbody id="orderDetail">
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-6"></div>
                    <div class="col-xs-6">
                        <table class="table">
                            <tr>
                                <th class="text-right" width="80%">Subtotal:</th>
                                <td class="text-right" width="20%">$<span id="sub_total">0.00</span></td>
                            </tr>
                            <tr>
                                <th class="text-right">Impuestos (18%)</th>
                                <td class="text-right">$<span id="taxes">0.00</span></td>
                            </tr>
                            <tr>
                                <th class="text-right">Impuesto de Ley (10%):</th>
                                <td class="text-right">$<span id="taxes_10">0.00</span></td>
                            </tr>
                            <tr>
                                <th class="text-right">Total:</th>
                                <td class="text-right">$<span id="total">0.00</span></td>
                            </tr>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
    </div>
</div>