{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Clientes')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Clientes
        <small>editar cliente</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mantenimientos</a></li>
        <li><a href="#">Clientes</a></li>
        <li class="active">Editar Cliente</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <div class="box box-purple">
                <div class="box-header">
                    <h3 class="box-title">Editando Cliente</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form id="form" method="post" action="{{ asset('/client/' . $current_item->id) }}" role="form"
                        enctype="multipart/form-data">
                        {{ csrf_field() }}
                        
                        <div class="box-body">
                            
                            @include('layouts.errors')
                            
                            <div class="form-group">
                                <label for="comercial_name">Nombre Comercial</label>
                                <input type="text" class="form-control" id="comercial_name" name="comercial_name"
                                       placeholder="Nombre Comercial" value="{{ $current_item->comercial_name }}">
                            </div>

                            <div class="form-group">
                                <label for="RNC">RNC</label>
                                <input type="text" class="form-control" id="RNC" name="RNC"
                                       placeholder="RNC" value="{{ $current_item->RNC }}">
                            </div>

                            <div class="form-group">
                                <label for="first_name">Nombres</label>
                                <input type="text" class="form-control" id="first_name" name="first_name"
                                placeholder="Nombres" value="{{ $current_item->first_name }}">
                            </div>
                            
                            <div class="form-group">
                                <label for="last_name">Apellidos</label>
                                <input type="text" class="form-control" id="last_name" name="last_name"
                                       placeholder="Apellidos" value="{{ $current_item->last_name }}">
                            </div>

                            <div class="form-group">
                                <label for="email">Correo Electrónico</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       placeholder="Correo Electrónico" value="{{ $current_item->email }}">
                            </div>

                            <div class="form-group">
                                <label for="birthdate">Fecha Nacimiento</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="birthdate" name="birthdate"
                                           value="{{ $current_item->birthdate }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="gender">Género / Sexo</label>
                                <select class="form-control select2" name="gender" id="gender">
                                    <option></option>
                                    <option value="M" @if($current_item->gender == 'M') selected="selected" @endif>
                                        Masculino
                                    </option>
                                    <option value="F" @if($current_item->gender == 'F') selected="selected" @endif>
                                        Femenino
                                    </option>
                                    <option value="C" @if($current_item->gender == 'C') selected="selected" @endif>
                                        Comercio
                                    </option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="cell_number">Celular</label>
                                <input type="text" class="form-control" id="cell_number" name="cell_number"
                                       placeholder="Celular" value="{{ $current_item->cell_number }}">
                            </div>

                            <div class="form-group">
                                <label for="phone">Teléfono</label>
                                <input type="text" class="form-control" id="phone" name="phone"
                                       placeholder="Teléfono" value="{{ $current_item->phone }}">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button id="patchForm" type="button" class="btn btn-black">Modificar</button>
                            <a href="{{ asset('/client') }}" class="btn btn-default">Cancelar</a>
                            <button id="destroyForm" type="button" class="btn btn-danger pull-right">Eliminar</button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-xs-8">

            @include('client.table')

        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    <script src="{{ asset('/js/bootstrap-datepicker.min.js') }}"></script>

    <script>
        //Manejando Formulario: update and delete
        var form = document.getElementById('form');
        var input = document.createElement("input");

        input.type = "hidden";
        input.name = "_method";

        form.onsubmit = function () {
            form.target = '_self';
        };

        document.getElementById('patchForm').onclick = function () {
            input.value = "PUT";
            form.appendChild(input);
            form.submit();
        };

        document.getElementById('destroyForm').onclick = function () {
            input.value = "DELETE";
            form.appendChild(input);
            form.submit();
        };

        $('#birthdate').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        });

        $('.select2').select2({
            placeholder: "Elija una opción"
        });
    </script>

    @stack('table_scripts')
@stop