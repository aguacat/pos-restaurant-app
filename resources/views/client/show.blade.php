{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Clientes')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Tipos de Alimentos
        <small>lista completa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mantenimientos</a></li>
        <li><a href="#">Tipos de Alimentos</a></li>
        <li class="active">Lista Tipos de Alimentos</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="box box-black">
                <div class="box-header">
                    <h3 class="box-title">Nuevo Cliente</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form method="post" action="{{ asset('/client') }}" role="form">
                        {{ csrf_field() }}

                        <div class="box-body">

                            @include('layouts.errors')

                            <div class="form-group">
                                <label for="comercial_name">Nombre Comercial</label>
                                <input type="text" class="form-control" id="comercial_name" name="comercial_name"
                                       placeholder="Nombre Comercial" value="{{ old('comercial_name') }}">
                            </div>

                            <div class="form-group">
                                <label for="RNC">RNC</label>
                                <input type="text" class="form-control" id="RNC" name="RNC"
                                       placeholder="RNC" value="{{ old('RNC') }}">
                            </div>

                            <div class="form-group">
                                <label for="first_name">Nombres</label>
                                <input type="text" class="form-control" id="first_name" name="first_name"
                                       placeholder="Nombres" value="{{ old('first_name') }}">
                            </div>

                            <div class="form-group">
                                <label for="last_name">Apellidos</label>
                                <input type="text" class="form-control" id="last_name" name="last_name"
                                       placeholder="Apellidos" value="{{ old('last_name') }}">
                            </div>

                            <div class="form-group">
                                <label for="email">Correo Electrónico</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       placeholder="Correo Electrónico" value="{{ old('email') }}">
                            </div>

                            <div class="form-group">
                                <label for="birthdate">Fecha Nacimiento</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="birthdate" name="birthdate">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="gender">Género / Sexo</label>
                                <select class="form-control select2" name="gender" id="gender">
                                    <option></option>
                                    <option value="M" @if(old('gender') == 'M') selected="selected" @endif>Masculino
                                    </option>
                                    <option value="F" @if(old('gender') == 'F') selected="selected" @endif>Femenino
                                    </option>
                                    <option value="C" @if(old('gender') == 'C') selected="selected" @endif>Comercio
                                    </option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="cell_number">Celular</label>
                                <input type="text" class="form-control" id="cell_number" name="cell_number"
                                       placeholder="Celular" value="{{ old('cell_number') }}">
                            </div>

                            <div class="form-group">
                                <label for="phone">Teléfono</label>
                                <input type="text" class="form-control" id="phone" name="phone"
                                       placeholder="Teléfono" value="{{ old('phone') }}">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-black">Guardar</button>
                            <a href="{{ asset('/client') }}" class="btn btn-default">Cancelar</a>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-8 col-xs-12">

            @include('client.table')

        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    <script src="{{ asset('/js/bootstrap-datepicker.min.js') }}"></script>

    {{--    <script src="{{ asset('/js/jquery.blockUI.js') }}"></script>--}}

    <script>
        $('#birthdate').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        });

        $('.select2').select2({
            placeholder: "Elija una opción"
        });
    </script>

    @stack('table_scripts')
@stop