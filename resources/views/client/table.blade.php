<div class="box box-black">
    <div class="box-header">
        <h3 class="box-title">Lista de Clientes</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <table id="datatable" class="table table-bordered table-hover table-responsive">
            <thead>
            <tr>
                <th>Nombre Contacto</th>
                <th>Correo</th>
                <th>Celular</th>
                <th>Teléfono</th>
                <th>RNC</th>
            </tr>
            </thead>
            @forelse($client_list as $client)
                <tr id="{{ $client->id }}"
                    @if(!empty($current_item) && $client->id == $current_item->id) class="black-mark" @endif>
                    <td>{{ $client->comercial_name OR $client->first_name.' '.$client->last_name }}</td>
                    <td>{{ $client->email }}</td>
                    <td>{{ $client->cell_number }}</td>
                    <td>{{ $client->phone }}</td>
                    <td>{{ $client->RNC }}</td>
                </tr>
            @empty
                <tr>
                    <td class="text-center" colspan="9">No hay clientes registrados.</td>
                </tr>
            @endforelse
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@push('table_scripts')
    <script>
        var current_item = "0";

        @isset($current_item)
            current_item = {{ $current_item->id }}
        @endisset

        $(document).ready(function () {
            $("#datatable tbody").on('click', 'tr', function () {
                if ($(this).closest('tr').attr('id') != undefined) {
                    window.location = "{{ asset('/client') }}/" + $(this).closest('tr').attr('id') + '/edit';
                }
            });

            $('#datatable').DataTable({
                info: false,
                searching: true,
                paging: true,
                ordering: false,
                lengthChange: false,
                createdRow: function (row) {
                    if (row.id == current_item) {
                        $(row.childNodes[1]).css('border-left', '0.3em solid #222d32');
                        $(row.childNodes[13]).css('border-right', '0.3em solid #222d32');
                    }
                }
            });
        });
    </script>
@endpush