<div class="box box-black">
    <div class="box-header">
        <h3 class="box-title">Lista de Estado de Orden</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <table id="datatable" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th width="100%">Estado de Orden</th>
            </tr>
            </thead>
            @forelse($order_status_list as $order_status)
                <tr id="{{ $order_status->id }}"
                    @if(!empty($current_item) && $order_status->id == $current_item->id) class="black-mark" @endif>
                    <td>{{ $order_status->order_status }}</td>
                </tr>
            @empty
                <tr>
                    <td class="text-center" colspan="2">No hay estados de orden registrados.</td>
                </tr>
            @endforelse
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@push('table_scripts')
    <script>
        $(document).ready(function () {
            $("#datatable tbody").on('click', 'tr', function () {
                if ($(this).closest('tr').attr('id') != undefined) {
                    window.location = "{{ asset('/order_status') }}/" + $(this).closest('tr').attr('id') + '/edit';
                }
            });
        });
    </script>
@endpush