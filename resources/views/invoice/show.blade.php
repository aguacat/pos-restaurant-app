{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Facturas')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Facturas
        <small>facturación</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Restaurante</a></li>
        <li><a href="#">Facturación</a></li>
        <li class="active">Factura</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <div class="box box-black">
                <div class="box-header">
                    <h3 class="box-title">Cuentas a Facturar</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form method="post" action="{{ asset('/user') }}" role="form">
                        {{ csrf_field() }}

                        <div class="box-body">

                            @include('layouts.errors')

                            <div class="form-group">
                                <label for="account">Órdenes</label>
                                <select class="form-control select2" name="account" id="account"
                                        onchange="loadOrderDetail($(this))">
                                    <option></option>
                                    @foreach($open_accounts_list as $account)
                                        <option value="{{ $account->id }}">{{ $account->name . ' | ' . $account->account }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label for="invoice_name">A nombre de:</label>
                                <input type="text" class="form-control" id="invoice_name" name="invoice_name"
                                       placeholder="Nombre de Cliente" value="" readonly>
                            </div>

                            <div id="div-rnc" class="form-group"></div>

                            <div class="form-group">
                                <label for="invoice_type">Tipo de Factura</label>
                                <select class="form-control select2" name="invoice_type" id="invoice_type"
                                        onchange="checkIfHasTax($(this))">
                                    @foreach($invoice_type_list as $invoice_type)
                                        <option value="{{ $invoice_type->id }}">{{ $invoice_type->invoice_type }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="in_brunch" name="in_brunch" checked="checked"
                                           disabled="disabled"> Para comer en el
                                    restaurante.
                                </label>
                            </div>

                            <div class="form-group">
                                <label for="email">Detalle de Factura</label>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th class="text-right" width="80%">Subtotal:</th>
                                            <td id="sub_total" class="text-right" width="20%">$0.00</td>
                                        </tr>
                                        <tr>
                                            <th class="text-right">Impuestos (18%)</th>
                                            <td id="taxes" class="text-right">$0.00</td>
                                        </tr>
                                        <tr>
                                            <th class="text-right">Impuesto de Ley (10%):</th>
                                            <td id="taxes_10" class="text-right">$0.00</td>
                                        </tr>
                                        <tr>
                                            <th class="text-right">Total:</th>
                                            <td id="total" class="text-right">$0.00</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button id="preinvoice_btn" type="button" class="btn btn-black" onclick="printPreInvoice()"
                                    disabled="disabled"><i class="fa fa-cutlery"></i></button>
                            <button id="processandprintinvoice_btn" type="button" class="btn btn-black"
                                    onclick="processPayment(true)" disabled="disabled"><i class="fa fa-print"></i>
                            </button>
                            <button id="processinvoice_btn" type="button" class="btn btn-black"
                                    onclick="processPayment()" disabled="disabled"><i class="fa fa-dollar"></i>
                            </button>
                            <a href="{{ asset('/invoice') }}" class="btn btn-default pull-right">Cancelar</a>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-xs-8">

            @include('invoice.detail')

        </div>
    </div>

    <div class="modal fade" id="printing-modal" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Información del Sistema</h4>
                </div>
                <div class="modal-body">
                    <p id="modal-message">Imprimiento Pre-Factura</p>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="process-invoice" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Facturación</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <form class="form-horizontal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="amount_paid" class="col-sm-4 control-label">Monto Pagado: </label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="amount_paid" placeholder="0.00">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="amount_to_cover" class="col-sm-4 control-label">Total a Pagar: </label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="amount_to_cover" placeholder="0.00"
                                               readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label id="amount_to_return_lbl" for="amount_to_return"
                                           class="col-sm-4 control-label">Devuelta: </label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="amount_to_return" placeholder="0.00"
                                               readonly>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="generateInvoice" type="button" class="btn btn-primary" disabled="disabled">
                            Procesar
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    {{--    <script src="{{ asset('/js/jquery.blockUI.js') }}"></script>--}}
    <script src="{{ asset('/js/custom.js') }}"></script>

    <script>
        var hasTax = true;
        var hasRNC = false;

        function enableInvoiceDetailInputs(status) {
            $('#invoice_name').prop('readonly', !status);
            $('#invoice_type');
            $('#in_brunch').prop('disabled', !status);
            $('#preinvoice_btn').prop('disabled', !status);
            $('#processandprintinvoice_btn').prop('disabled', !status);
            $('#processinvoice_btn').prop('disabled', !status);
            $('#amount_paid').val('');
            $('#amount_to_return').val('');
            document.getElementById("amount_to_return").style.color = '#000000';
            document.getElementById("amount_to_return_lbl").style.color = '#000000';
            $('#generateInvoice').prop('disabled', true);
        }

        function calulateTotals(price_due) {
            price_due = price_due || 0.00;

            var taxes = ((price_due / 1.18) * 0.18);

            if (!hasTax) {
                taxes = 0.00;
            }

            var taxes_10 = ((price_due / 1.10) * 0.10)

            if (!$('#in_brunch').is(':checked')) {
                taxes_10 = 0.00;
            }

            var sub_total = price_due / (1.18);
            var total = sub_total + taxes + taxes_10;

            $('#sub_total').html('$' + Math.round(sub_total).toFixed(2));
            $('#taxes').html('$' + Math.round(taxes).toFixed(2));
            $('#taxes_10').html('$' + Math.round(taxes_10).toFixed(2));
            $('#total').html('$' + Math.round(total).toFixed(2));
        }

        function loadOrderDetail(element, hasTax) {
            hasTax = hasTax || false;
            var id_account = element.find(':selected').val();

            $('#sub_total').html('$' + '0.00');
            $('#taxes').html('$' + '0.00');
            $('#taxes_10').html('$' + '0.00');
            $('#total').html('$' + '0.00');

            $.post('{{ asset('/invoice/') }}/' + id_account + '/accountOrders', {
                _token: "{{ csrf_token() }}",
            })
                .done(function (data) {
                    $('#orderDetail').empty();

                    $.each(data.orderDetails, function (index, value) {
                        var columna1 = '<td class="text-center"> 01 </td>';
                        var columna2 = '<td class="item_name">' + value.name + '</td>';
                        var columna3 = '<td class="text-right item_price">' + value.price + '</td>';
                        var fila = '<tr id="' + value.id + '">' + columna1 + columna2 + columna3 + '</tr>';

                        $('#orderDetail').append(fila);

                        enableInvoiceDetailInputs(true);
                        calulateTotals(data.totals.total_price, hasTax);
                    });

                    var orderName = element.find(':selected').text();
                    orderName = orderName.substring(9, orderName.length)
                    $('#invoice_name').val(capitalizeFirstLetter(orderName));
                });
        }

        function checkIfHasTax(element) {
            var id_invoice_type = element.find(':selected').val();

            $.post('{{ asset('/invoice/') }}/' + id_invoice_type + '/checkTax', {
                _token: "{{ csrf_token() }}",
            })
                .done(function (data) {
                    hasTax = (data.has_tax == 1) ? true : false;
                    hasRNC = (data.rnc_required == 1) ? true : false;

                    $('#div-rnc').empty();

                    if (data.rnc_required) {
                        var label = '<label for="invoice_rnc">RNC:</label>';
                        var field = '<input type="text" class="form-control" id="invoice_rnc" name="invoice_rnc" placeholder="RNC" value="">';
                        var component = label + field;

                        $('#div-rnc').append(component);
                    }

                    loadOrderDetail($('#account'), hasTax);
                });
        }

        function printPreInvoice() {
            var cashier_name = '{{ auth()->guard('web')->user()->first_name . ' ' . auth()->guard('web')->user()->last_name }}';
            var invoice_name = $('#invoice_name').val();
            var invoice_type = $('#invoice_type').find(':selected').text();
            var sub_total = $('#sub_total').html().replace('$', '');
            var taxes = $('#taxes').html().replace('$', '');
            var taxes_10 = $('#taxes_10').html().replace('$', '');
            var total = $('#total').html().replace('$', '');
            var items = [];

            if (invoice_name.length == 0) {
                return;
            }

            $('#modal-message').html('Imprimiendo pre-factura.');
            $('#printing-modal').modal('show');

            $('#orderDetail tr').each(function (item, value) {
                var item_name = $(value).find('.item_name').html();
                var item_price = $(value).find('.item_price').html();

                var item = {};
                item.item_name = item_name;
                item.item_price = item_price;

                items.push(item);
            });

            $.post('https://localhost/print/pre_invoice.php', {
                cashier_name: cashier_name,
                invoice_name: invoice_name,
                invoice_type: invoice_type,
                sub_total: sub_total,
                taxes: taxes,
                taxes_10: taxes_10,
                total: total,
                items: JSON.stringify(items)
            })
                .done(function (data) {
                    $('#printing-modal').modal('hide')
                });
        }

        function processPayment(print) {
            print = print || false;
            $('#processinvoice_btn').prop('disabled', true);
            $('#amount_paid').val('');
            $('#amount_to_return').val('');
            var total = $('#total').html().replace('$', '');

            $('#amount_to_cover').val(total);

            document.getElementById('generateInvoice').addEventListener("click", function () {
                generateInvoice(print);
            }, false);

            $('#process-invoice').modal('show');
        }

        $('#amount_paid').keyup(function () {
            this.value = this.value.replace(/[^0-9\.,]/g, '');

            var amount_paid = $('#amount_to_cover').val();
            var amount_to_return = this.value - amount_paid.replace(',', '');

            if (amount_to_return < 0) {
                document.getElementById("amount_to_return").style.color = '#d33724';
                document.getElementById("amount_to_return_lbl").style.color = '#d33724';
                $('#generateInvoice').prop('disabled', true);
            } else {
                document.getElementById("amount_to_return").style.color = '#357ca5';
                document.getElementById("amount_to_return_lbl").style.color = '#357ca5';
                $('#generateInvoice').prop('disabled', false);
            }

            $('#amount_to_cover').val(formatNumber(amount_paid));
            $('#amount_to_return').val(formatNumber(amount_to_return.toFixed(2)));
        });

        function generateInvoice(print) {
            var id_account = $('#account').find(':selected').val();
            $('#process-invoice').modal('hide');

            if (print) {
                $('#modal-message').html('Procesando e imprimiendo factura.');
            } else {
                $('#modal-message').html('Procesando factura.');
            }

            $('#printing-modal').modal('show');

            var cashier_name = '{{ auth()->guard('web')->user()->first_name . ' ' . auth()->guard('web')->user()->last_name }}';
            var invoice_name = $('#invoice_name').val();
            var invoice_type = $('#invoice_type').find(':selected').val();
            var sub_total = $('#sub_total').html().replace('$', '');
            var taxes = $('#taxes').html().replace('$', '');
            var taxes_10 = $('#taxes_10').html().replace('$', '');
            var total = $('#total').html().replace('$', '');
            var amount_paid = $('#amount_paid').val();
            var amount_to_return = $('#amount_to_return').val();
            var items = [];
            var rnc = '';

            if ($('#invoice_rnc').is(':visible')) {
                rnc = $('#invoice_rnc').val();
            }

            $('#orderDetail tr').each(function (item, value) {
                var item_name = $(value).find('.item_name').html();
                var item_price = $(value).find('.item_price').html();

                var item = {};
                item.item_name = item_name;
                item.item_price = item_price;

                items.push(item);
            });

            $.post('{{ asset('/invoice') }}/' + id_account + '/processInvoice', {
                _token: "{{ csrf_token() }}",
                cashier_name: cashier_name,
                invoice_name: invoice_name,
                rnc: rnc,
                invoice_type: invoice_type,
                sub_total: sub_total,
                taxes: taxes,
                taxes_10: taxes_10,
                total: total,
                amount_paid: amount_paid,
                amount_to_return: amount_to_return,
                items: JSON.stringify(items)
            })
                .done(function (data) {
                    var ncf = data.invoice.NCF;
                    var rnc = data.invoice.RNC;

                    $('#processinvoice_btn').prop('disabled', false);

                    //If Print == true, I need to print the invoice.
                    if (print) {
                        $.post('https://localhost/print/invoice.php', {
                            cashier_name: cashier_name,
                            invoice_name: invoice_name,
                            ncf: data.invoice.NCF,
                            rnc: data.invoice.RNC,
                            invoice_type: invoice_type,
                            sub_total: sub_total,
                            taxes: taxes,
                            taxes_10: taxes_10,
                            total: total,
                            items: JSON.stringify(items)
                        })
                            .done(function (data) {
                                $('#printing-modal').modal('hide')
                            });
                    }

                    window.location = '{{ asset('/invoice') }}';
                });
        }

        $('#in_brunch').on('change', function () {
            checkIfHasTax($('#invoice_type'));
        });

        $('.select2').select2({
            placeholder: "Elija una opción",
            readonly: true
        });
    </script>

    @stack('table_scripts')
@stop