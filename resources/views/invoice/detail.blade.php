<div class="box box-black">
    <div class="box-header">
        <h3 class="box-title">Detalle de Factura</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <i class="fa fa-globe"></i> Brunch Café Bistro
                        <small class="pull-right">Fecha: {{ date('d-m-Y') }}</small>
                    </h2>
                </div>
                <!-- /.col -->
            </div>

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table id="items-table" class="table table-striped">
                        <thead>
                        <tr>
                            <th class="text-center" width="10%">Cantidad</th>
                            <th class="text-left" width="75%">Producto</th>
                            <th class="text-right" width="15%">Sub-Total</th>
                        </tr>
                        </thead>
                        <tbody id="orderDetail">
                        <tr>
                            <td class="text-center" colspan="3">
                                Escoja una orden para ver el detalle.
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@push('table_scripts')
    <script>
    </script>
@endpush