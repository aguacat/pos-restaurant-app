<div class="box box-black">
    <div class="box-header">
        <h3 class="box-title">Lista de Tipos de Alimentos</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <table id="datatable" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th width="90%">Nombre</th>
                <th width="10%">Capacidad</th>
            </tr>
            </thead>
            @forelse($table_list as $table)
                <tr id="{{ $table->id }}"
                    @if(!empty($current_item) && $table->id == $current_item->id) class="black-mark" @endif>
                    <td>{{ $table->name }}</td>
                    <td>{{ $table->capacity }}</td>
                </tr>
            @empty
                <tr>
                    <td class="text-center" colspan="2">No hay mesas registradas.</td>
                </tr>
            @endforelse
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@push('table_scripts')
    <script>
        $(document).ready(function () {
            $("#datatable tbody").on('click', 'tr', function () {
                if ($(this).closest('tr').attr('id') != undefined) {
                    window.location = "{{ asset('/table') }}/" + $(this).closest('tr').attr('id') + '/edit';
                }
            });
        });
    </script>
@endpush