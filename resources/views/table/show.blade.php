{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Mesas')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Mesas
        <small>lista completa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mantenimientos</a></li>
        <li><a href="#">Mesas</a></li>
        <li class="active">Lista de Mesas</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <div class="box box-black">
                <div class="box-header">
                    <h3 class="box-title">Nueva Mesa</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form method="post" action="{{ asset('/table') }}" role="form">
                        {{ csrf_field() }}

                        <div class="box-body">

                            @include('layouts.errors')

                            <div class="form-group">
                                <label for="name">Número de Mesa</label>
                                <input type="text" class="form-control" id="name" name="name"
                                       placeholder="Número de Mesa" value="{{ old('name') }}">
                            </div>

                            <div class="form-group">
                                <label for="capacity">Capacidad</label>
                                <input type="text" class="form-control" id="capacity" name="capacity"
                                       placeholder="Capacidad" value="{{ old('capacity') }}">
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-black">Guardar</button>
                            <button type="reset" class="btn btn-cancel pull-right">Cancelar</button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-xs-8">

            @include('table.table')

        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    {{--    <script src="{{ asset('/js/jquery.blockUI.js') }}"></script>--}}

    <script>
        $('.select2').select2({
            placeholder: "Elija una opción"
        });
    </script>

    @stack('table_scripts');
@stop