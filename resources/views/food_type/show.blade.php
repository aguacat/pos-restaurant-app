{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Tipos de Alimentos')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Tipos de Alimentos
        <small>lista completa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mantenimientos</a></li>
        <li><a href="#">Tipos de Alimentos</a></li>
        <li class="active">Lista Tipos de Alimentos</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <div class="box box-black">
                <div class="box-header">
                    <h3 class="box-title">Nuevo Tipo de Alimento</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form method="post" action="{{ asset('/food_type') }}" role="form">
                        {{ csrf_field() }}

                        <div class="box-body">

                            @include('layouts.errors')

                            <div class="form-group">
                                <label for="food_type">Tipo de Alimento</label>
                                <input type="text" class="form-control" id="food_type" name="food_type"
                                       placeholder="Tipo de Alimento" value="{{ old('food_type') }}">
                            </div>

                            <div class="form-group">
                                <label for="cue_type">Tipo de Cola</label>
                                <select class="form-control select2" name="cue_type" id="cue_type">
                                    <option></option>
                                    <option value="A" @if(old('cue_type') == 'A') selected="selected" @endif>Alimentos
                                    </option>
                                    <option value="B" @if(old('cue_type') == 'B') selected="selected" @endif>Bebidas
                                    </option>
                                </select>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-black">Guardar</button>
                            <button type="reset" class="btn btn-cancel pull-right">Cancelar</button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-xs-8">

            @include('food_type.table')

        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    {{--    <script src="{{ asset('/js/jquery.blockUI.js') }}"></script>--}}

    <script>
        $('.select2').select2({
            placeholder: "Elija una opción"
        });
    </script>

    @stack('table_scripts');
@stop