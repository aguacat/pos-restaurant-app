<div class="box box-black">
    <div class="box-header">
        <h3 class="box-title">Lista de Tipos de Alimentos</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <table id="datatable" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th width="80%">Tipo de Alimento</th>
                <th width="20%">Tipo de Cola</th>
            </tr>
            </thead>
            @forelse($food_type_list as $food_type)
                <tr id="{{ $food_type->id }}"
                    @if(!empty($current_item) && $food_type->id == $current_item->id) class="black-mark" @endif>
                    <td>{{ $food_type->food_type }}</td>
                    <td>{{ ($food_type->cue_type == 'A') ? 'Alimentos' : 'Bebidas'  }}</td>
                </tr>
            @empty
                <tr>
                    <td class="text-center" colspan="2">No hay tipos de alimentos registrados.</td>
                </tr>
            @endforelse
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@push('table_scripts')
    <script>
        $(document).ready(function () {
            $("#datatable tbody").on('click', 'tr', function () {
                if ($(this).closest('tr').attr('id') != undefined) {
                    window.location = "{{ asset('/food_type') }}/" + $(this).closest('tr').attr('id') + '/edit';
                }
            });
        });
    </script>
@endpush