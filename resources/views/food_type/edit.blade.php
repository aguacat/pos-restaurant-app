{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Edición Tipo de Alimento')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Tipos de Alimentos
        <small>editar tipo de alimento</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mantenimientos</a></li>
        <li><a href="#">Tipos de Alimentos</a></li>
        <li class="active">Editar Alimento</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <div class="box box-purple">
                <div class="box-header">
                    <h3 class="box-title">Editando Tipo de Alimento</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form id="form" method="post" action="{{ asset('/food_type/' . $current_item->id) }}" role="form">
                        {{ csrf_field() }}

                        <div class="box-body">

                            @include('layouts.errors')

                            <div class="form-group">
                                <label for="food_type">Tipo de Alimento</label>
                                <input type="text" class="form-control" id="food_type" name="food_type"
                                       placeholder="Tipo de Alimento" value="{{ $current_item->food_type }}">
                            </div>

                            <div class="form-group">
                                <label for="cue_type">Tipo de Cola</label>
                                <select class="form-control select2" name="cue_type" id="cue_type">
                                    <option></option>
                                    <option value="A" @if($current_item->cue_type == 'A') selected="selected" @endif>
                                        Alimentos
                                    </option>
                                    <option value="B" @if($current_item->cue_type == 'B') selected="selected" @endif>
                                        Bebidas
                                    </option>
                                </select>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input name="active" checked="checked" type="checkbox"> Activo
                                </label>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button id="patchForm" type="button" class="btn btn-black">Modificar</button>
                            <a href="{{ asset('/food_type') }}" class="btn btn-default">Cancelar</a>
                            <button id="destroyForm" type="button" class="btn btn-danger pull-right">Eliminar</button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-xs-8">

            @include('food_type.table')

        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    <script>
        //Manejando Formulario: update and delete
        var form = document.getElementById('form');
        var input = document.createElement("input");

        input.type = "hidden";
        input.name = "_method";

        form.onsubmit = function () {
            form.target = '_self';
        };

        document.getElementById('patchForm').onclick = function () {
            input.value = "PUT";
            form.appendChild(input);
            form.submit();
        };

        document.getElementById('destroyForm').onclick = function () {
            input.value = "DELETE";
            form.appendChild(input);
            form.submit();
        };

        $('.select2').select2({
            placeholder: "Elija una opción"
        });
    </script>

    @stack('table_scripts')
@stop