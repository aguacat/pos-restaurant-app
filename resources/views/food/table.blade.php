<div class="box box-black">
    <div class="box-header">
        <h3 class="box-title">Lista de Menú</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <table id="datatable" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th width="25%">Nombre</th>
                <th width="25%">Ingredientes</th>
                <th width="10%">Precio</th>
                <th width="10%">Costo</th>
                <th width="20%">Tipo</th>
                <th width="10%">Imagen</th>

            </tr>
            </thead>
            <tbody>
            @forelse($food_list as $food)
                <tr id="{{ $food->id }}">
                    <td>{{ $food->name }}</td>
                    <td>{{ $food->ingredients }}</td>
                    <td>{{ $food->price }}</td>
                    <td>{{ $food->cost }}</td>
                    <td>{{ $food->food_type->food_type }}</td>
                    <td>
                        <button type="button" class="btn btn-xs btn-default" onclick="loadPicture($(this))">
                            Ver Imagen
                        </button>
                    </td>
                </tr>
            @empty
                <tr>
                    <td class="text-center" colspan="6">No hay elementos de menú registrados.</td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <th width="25%">Nombre</th>
                <th width="43%">Ingredientes</th>
                <th width="8%">Precio</th>
                <th width="8%">Costo</th>
                <th width="8%">Tipo</th>
                <th width="8%">Imagen</th>

            </tr>
            </tfoot>
        </table>

        <div class="modal fade bs-example-modal-sm" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Logo:</h4>
                    </div>
                    <div class="modal-body">
                        <img id="imagen_logo" src="" class="img-responsive center-block" alt="Imagen de Menú">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

@push('table_scripts')
    <script>
        var current_item = "0";

        @isset($current_item)
            current_item = {{ $current_item->id }}
        @endisset

        $(document).ready(function () {
            $("#datatable tbody").on('click', 'tr td:not(:nth-child(6))', function () {
                if ($(this).closest('tr').attr('id') != undefined) {
                    window.location = "{{ asset('/food') }}/" + $(this).closest('tr').attr('id') + '/edit';
                }
            });

            $('#datatable').DataTable({
                info: false,
                searching: true,
                paging: true,
                ordering: false,
                lengthChange: false,
                createdRow: function (row) {
                    if (row.id == current_item) {
                        $(row.childNodes[1]).css('border-left', '0.3em solid #222d32');
                        $(row.childNodes[11]).css('border-right', '0.3em solid #222d32');
                    }
                }
            });
        });

        function loadPicture(button) {
            var id = button.closest('tr').prop('id'); //.getAttribute('id');

            $.get('{{ asset('/food') }}/' + id)
                .done(function (data) {
                    $('#imagen_logo').prop('src', data);
                    $('#modal').modal('show')
                });
        }
    </script>
@endpush