{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Menú')

@section('content_header')
    <!-- Content Header (Page header) -->
    <h1>Menú
        <small>lista completa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mantenimientos</a></li>
        <li><a href="#">Menú</a></li>
        <li class="active">Lista de Menú</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-4">
            <div class="box box-black">
                <div class="box-header">
                    <h3 class="box-title">Nuevo Elemento de Menú</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form method="post" action="{{ asset('/food') }}" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="box-body">

                            @include('layouts.errors')

                            <div class="form-group">
                                <label for="name">Nombre</label>
                                <input type="text" class="form-control" id="name" name="name"
                                       placeholder="Nombre" value="{{ old('name') }}">
                            </div>

                            <div class="form-group">
                                <label for="ingredients">Ingredientes</label>
                                <input type="text" class="form-control" id="ingredients" name="ingredients"
                                       placeholder="Ingredientes" value="{{ old('ingredients') }}">
                            </div>

                            <div class="form-group">
                                <label for="food_type">Tipo de Alimento</label>
                                <select class="form-control select2" name="food_type" id="food_type">
                                    <option></option>
                                    @foreach($food_type_list as $food_type)
                                        <option value="{{ $food_type->id }}"
                                                @if(old('food_type')) selected="selected" @endif>{{ $food_type->food_type }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="price">Precio</label>
                                <input type="text" class="form-control" id="price" name="price"
                                       placeholder="Precio" value="{{ old('price') }}">
                            </div>

                            <div class="form-group">
                                <label for="cost">Costo</label>
                                <input type="text" class="form-control" id="cost" name="cost"
                                       placeholder="Costo" value="{{ old('cost') }}">
                            </div>

                            <div class="form-group">
                                <label for="logo">Imagen</label>
                                <input type="file" id="imagen" name="image">
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="active" checked="checked"> Activo
                                </label>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-black">Guardar</button>
                            <button type="reset" class="btn btn-cancel pull-right">Cancelar</button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-xs-8">

            @include('food.table')

        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
@stop

@section('js')
    {{--    <script src="{{ asset('/js/jquery.blockUI.js') }}"></script>--}}

    <script>
        $('.select2').select2({
            placeholder: "Elija una opción"
        });
    </script>

    @stack('table_scripts');
@stop